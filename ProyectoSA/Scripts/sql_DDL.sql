﻿

CREATE TABLE Cuenta
  (
    idCuenta  INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    dpi       DECIMAL (14) NOT NULL ,
    nombre    VARCHAR (128) NOT NULL ,
    direccion VARCHAR (256) NOT NULL ,
    telefono  VARCHAR (16) ,
    saldo     DECIMAL (18,2) NOT NULL ,
    estado    DECIMAL (2) NOT NULL ,
    convenio  INTEGER NOT NULL
  ) ;

CREATE TABLE PrestamoAuto
  (
    idPrestamoAuto INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    modelo         DECIMAL (4) NOT NULL ,
    placa          VARCHAR (12) NOT NULL ,
    chasis         VARCHAR (24) NOT NULL ,
    monto          DECIMAL (16,2) NOT NULL ,
    numPagos       INTEGER NOT NULL ,
    interes        DECIMAL (2) NOT NULL ,
    estado         DECIMAL (2) NOT NULL ,
    convenio       INTEGER NOT NULL
  ) ;

CREATE TABLE PrestamoAutoCatalogo
  (
    idPrestamoAutoCatalogo INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    descripcion            VARCHAR (1024) NOT NULL ,
    aniominimo             DECIMAL (4) NOT NULL ,
    aniomaximo             DECIMAL (4) NOT NULL ,
    preciobasemin          DECIMAL (16,2) NOT NULL ,
    preciobasemax          DECIMAL (16,2) NOT NULL ,
    porcentaje             DECIMAL (2) NOT NULL
  ) ;

CREATE TABLE PrestamoHipoteca
  (
    idPrestamoHipoteca INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    descripcion        VARCHAR (128) NOT NULL ,
    area               DECIMAL (12) NOT NULL ,
    direccion          VARCHAR (256) NOT NULL ,
    monto              DECIMAL (16,2) NOT NULL ,
    numPagos           INTEGER NOT NULL ,
    interes            DECIMAL (2) NOT NULL ,
    estado             DECIMAL (2) NOT NULL ,
    convenio           INTEGER NOT NULL
  ) ;

CREATE TABLE PrestamoHipotecaCatalogo
  (
    idPrestamoHCatalogo INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    descripcion         VARCHAR (1024) NOT NULL ,
    aniominimo          DECIMAL (4) NOT NULL ,
    aniomaximo          DECIMAL (4) NOT NULL ,
    preciobasemin       DECIMAL (16,2) NOT NULL ,
    preciobasemax       DECIMAL (16,2) NOT NULL ,
    porcentaje          DECIMAL (2) NOT NULL
  ) ;

CREATE TABLE Seguro
  (
    idSeguro     INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    prima        INTEGER NOT NULL ,
    cuota        DECIMAL (16,2) NOT NULL ,
    nocuotas     INTEGER NOT NULL ,
    vigencia     INTEGER NOT NULL ,
    dinero       DECIMAL (16,2) NOT NULL ,
    beneficiario VARCHAR (256) NOT NULL ,
    estado       DECIMAL (2) NOT NULL ,
    idTipoSeguro INTEGER NOT NULL
  ) ;

CREATE TABLE TipoSeguro
  (
    idTipoSeguro INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre       VARCHAR (256) NOT NULL ,
    descripcion  VARCHAR (2048)
  ) ;

CREATE TABLE TipoTransaccion
  (
    idTipoTransaccion INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre            VARCHAR (64) NOT NULL ,
    descripcion       VARCHAR (2048)
  ) ;

CREATE TABLE Transaccion
  (
    idTransaccion      INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    fecha              DATE NOT NULL ,
    monto              DECIMAL (16,2) NOT NULL ,
    descripcion        VARCHAR (2048) NOT NULL ,
    idPrestamoHipoteca INTEGER ,
    idPrestamoAuto     INTEGER ,
    idTipoTransaccion  INTEGER NOT NULL ,
    idCuenta           INTEGER ,
    idUsuarioSeguro    INTEGER
  ) ;

CREATE TABLE Usuario
  (
    convenio  INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    dpi       DECIMAL (14) NOT NULL ,
    nombre1   VARCHAR (32) NOT NULL ,
    nombre2   VARCHAR (32) NOT NULL ,
    apellido1 VARCHAR (32) NOT NULL ,
    apellido2 VARCHAR (32) NOT NULL ,
    passwd    CHAR (128) NOT NULL ,
    estado    DECIMAL (2) NOT NULL
  ) ;

CREATE TABLE UsuarioSeguro
  (
    idUsuarioSeguro INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    fecha           DATE NOT NULL ,
    convenio        INTEGER NOT NULL ,
    idSeguro        INTEGER NOT NULL
  ) ;

ALTER TABLE Cuenta ADD CONSTRAINT Cuenta_Usuario_FK FOREIGN KEY ( convenio ) REFERENCES Usuario ( convenio ) ;

ALTER TABLE PrestamoAuto ADD CONSTRAINT PrestamoAuto_Usuario_FK FOREIGN KEY ( convenio ) REFERENCES Usuario ( convenio ) ;

ALTER TABLE PrestamoHipoteca ADD CONSTRAINT PrestamoHipoteca_Usuario_FK FOREIGN KEY ( convenio ) REFERENCES Usuario ( convenio ) ;

ALTER TABLE Seguro ADD CONSTRAINT Seguro_TipoSeguro_FK FOREIGN KEY ( idTipoSeguro ) REFERENCES TipoSeguro ( idTipoSeguro ) ;

ALTER TABLE Transaccion ADD CONSTRAINT Transaccion_Cuenta_FK FOREIGN KEY ( idCuenta ) REFERENCES Cuenta ( idCuenta ) ;

ALTER TABLE Transaccion ADD CONSTRAINT Transaccion_PH_FK FOREIGN KEY ( idPrestamoHipoteca ) REFERENCES PrestamoHipoteca ( idPrestamoHipoteca ) ;

ALTER TABLE Transaccion ADD CONSTRAINT Transaccion_PrestamoAuto_FK FOREIGN KEY ( idPrestamoAuto ) REFERENCES PrestamoAuto ( idPrestamoAuto ) ;

ALTER TABLE Transaccion ADD CONSTRAINT Transaccion_TT_FK FOREIGN KEY ( idTipoTransaccion ) REFERENCES TipoTransaccion ( idTipoTransaccion ) ;

ALTER TABLE Transaccion ADD CONSTRAINT Transaccion_US_FK FOREIGN KEY ( idUsuarioSeguro ) REFERENCES UsuarioSeguro ( idUsuarioSeguro ) ;

ALTER TABLE UsuarioSeguro ADD CONSTRAINT UsuarioSeguro_S_FK FOREIGN KEY ( idSeguro ) REFERENCES Seguro ( idSeguro ) ;

ALTER TABLE UsuarioSeguro ADD CONSTRAINT UsuarioSeguro_U_FK FOREIGN KEY ( convenio ) REFERENCES Usuario ( convenio ) ;


INSERT INTO TipoSeguro (nombre, descripcion) VALUES ('Vida', 'Seguro de vida');
INSERT INTO TipoSeguro (nombre, descripcion) VALUES ('Auto', 'Seguro de automovil');

INSERT INTO TipoTransaccion (nombre, descripcion) VALUES ('Deposito','Deposito');
INSERT INTO TipoTransaccion (nombre, descripcion) VALUES ('Retiro','Retiro');
INSERT INTO TipoTransaccion (nombre, descripcion) VALUES ('Pago','Pago');

INSERT INTO PrestamoAutoCatalogo VALUES (0,'Prestamo Basico',1990,2014,1000,20000,40),(0,'Prestamo Avanzado',1990,2014,20000,200000,80);

INSERT INTO PrestamoHipotecaCatalogo VALUES (0,'Prestamo Basico',20,200,1000,20000,40),(0,'Prestamo Avanzado',200,2000,20000,200000,80);

INSERT INTO Seguro VALUES (1,30,1500,20,20,15000,'',1,1);
