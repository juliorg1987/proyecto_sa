/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proyecto.sa.bean;

import ejb.BeanControlAuth;
import ejb.TipoSeguro;
import ejb.TipoSeguroFacadeLocal;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import proyecto.sa.util.UsuarioAuth;

/**
 *
 * @author victor
 */
@ManagedBean
@RequestScoped
public class TipoSeguroBean {
    
    @EJB
    private BeanControlAuth beanControlAuth;
    
    @EJB
    private TipoSeguroFacadeLocal tipoSeguroFacade;

    
    /**
     * Creates a new instance of TipoSeguroBean
     */
    public TipoSeguroBean() {
    }
    
    public String getUsuarios(){
        String salida = "";
        for (String token: beanControlAuth.getUsuarios().keySet()) {
            UsuarioAuth usuarioAuth = beanControlAuth.getUsuarios().get(token);
            salida = token + ";"+usuarioAuth.getNombre()+"--------------";
        }
        return salida;
    }
    
}
