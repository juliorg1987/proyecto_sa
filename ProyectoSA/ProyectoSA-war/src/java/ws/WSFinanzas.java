/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ws;

import ejb.BeanControlAuth;
import ejb.Cuenta;
import ejb.CuentaFacadeLocal;
import ejb.PrestamoAuto;
import ejb.PrestamoAutoCatalogo;
import ejb.PrestamoAutoCatalogoFacadeLocal;
import ejb.PrestamoAutoFacadeLocal;
import ejb.PrestamoHipoteca;
import ejb.PrestamoHipotecaCatalogo;
import ejb.PrestamoHipotecaCatalogoFacadeLocal;
import ejb.PrestamoHipotecaFacadeLocal;
import ejb.Seguro;
import ejb.SeguroFacadeLocal;
import ejb.TipoTransaccionFacadeLocal;
import ejb.Transaccion;
import ejb.TransaccionFacadeLocal;
import ejb.Usuario;
import ejb.UsuarioFacadeLocal;
import ejb.UsuarioSeguro;
import ejb.UsuarioSeguroFacadeLocal;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author victor
 */
@WebService(serviceName = "WSFinanzas")
@Stateless()
public class WSFinanzas {
    @EJB
    private UsuarioSeguroFacadeLocal usuarioSeguroFacade;
    @EJB
    private UsuarioFacadeLocal usuarioFacade;
    @EJB
    private TransaccionFacadeLocal transaccionFacade;
    @EJB
    private TipoTransaccionFacadeLocal tipoTransaccionFacade;
    @EJB
    private SeguroFacadeLocal seguroFacade;
    @EJB
    private PrestamoHipotecaFacadeLocal prestamoHipotecaFacade;
    @EJB
    private PrestamoHipotecaCatalogoFacadeLocal prestamoHipotecaCatalogoFacade;
    @EJB
    private PrestamoAutoFacadeLocal prestamoAutoFacade;
    @EJB
    private PrestamoAutoCatalogoFacadeLocal prestamoAutoCatalogoFacade;
    @EJB
    private CuentaFacadeLocal cuentaFacade;
    @EJB
    private BeanControlAuth beanControlAuth;

    /**
     * Web service operation
     */
    @WebMethod(operationName = "autenticar")
    public String autenticar(@WebParam(name = "convenio") int convenio, @WebParam(name = "password") String password) {
        //TODO write your implementation code here:
        Usuario usr = usuarioFacade.auntenticar(convenio, password);
        if (usr != null) {
            //autenticar...
            String token = beanControlAuth.putUsuarioLogueado(usr);
            return "<respuesta><valido>true</valido><mensaje>Valido</mensaje><token>" + token + "</token></respuesta>";
        } else {
            //error
            return "<respuesta><valido>false</valido><mensaje>El usuario o contrasenia son invalidos.</mensaje><token></token></respuesta>";
        }
    }

    /**
     * Web service operation
     *
     * SIMULAR 3 SERVICIOS - BPM
     *
     *
     */
    @WebMethod(operationName = "crearConvenio")
    public String crearConvenio(@WebParam(name = "dpi") long dpi, 
            @WebParam(name = "nombre1") String nombre1, @WebParam(name = "nombre2") String nombre2, 
            @WebParam(name = "apellido1") String apellido1, 
            @WebParam(name = "apellido2") String apellido2, @WebParam(name = "password") String password) {
        //TODO write your implementation code here:
        Usuario usr = usuarioFacade.findByDPI(dpi);
        if (usr != null) {
            //regresar error, ya se registro...
            return "<respuesta><valido>false</valido><mensaje>Usuario ya existe.</mensaje></respuesta>";
        } else {
            //creando el usuario
            usr = new Usuario(null, dpi, nombre1, nombre2, apellido1, apellido2, password, (short) 1);
            usuarioFacade.create(usr);
            return "<respuesta><valido>true</valido><mensaje>" + usr.getConvenio() + "</mensaje></respuesta>";
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "aperturaDeCuenta")
    public String aperturaDeCuenta(@WebParam(name = "token") String token, 
            @WebParam(name = "dpi") long dpi, 
            @WebParam(name = "nombre") String nombre, 
            @WebParam(name = "direccion") String direccion, @WebParam(name = "telefono") String telefono) {
        //TODO write your implementation code here:
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Usuario usr = usuarioFacade.findByDPI(dpi);
            if (usr != null) {
                //aperturando la nueva cuenta
                Cuenta cuenta = new Cuenta(null, dpi, nombre, direccion, new BigDecimal(0), (short) 1);
                cuenta.setConvenio(usr);
                cuentaFacade.create(cuenta);
                return "<respuesta><valido>true</valido><mensaje>Valido.</mensaje><numcuenta>" + cuenta.getIdCuenta() + "</numcuenta></respuesta>";
            } else {
                return "<respuesta><valido>false</valido><mensaje>El usuario no existe.</mensaje><numcuenta></numcuenta></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>El usuario no se ha autenticado.</mensaje><numcuenta></numcuenta></respuesta>";
        }
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "cuentasPorConvenio")
    public String cuentasPorConvenio(@WebParam(name = "token") String token, @WebParam(name = "convenio") int convenio) {
        //TODO write your implementation code here:
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Usuario usr = usuarioFacade.find(convenio);
            if (usr != null) {
                //aperturando la nueva cuenta
                List<Cuenta> listaCuentas = usr.getCuentaList(); //cuentaFacade.cuentasPorConvenio(convenio);
                if (listaCuentas != null && listaCuentas.size() > 0) {
                    String respuesta = "<respuesta><valido>false</valido><cuentas>";
                    for (Cuenta c: listaCuentas) {
                        respuesta += "<cuenta><numero>"+c.getIdCuenta()+"</numero><saldo>"+c.getSaldo()+"</saldo></cuenta>";
                    }
                    respuesta += "</cuentas></respuesta>";
                    return respuesta;
                } else {
                    return "<respuesta><valido>false</valido><cuentas>El usuario no posee cuentas.</cuentas></respuesta>";
                }
            } else {
                return "<respuesta><valido>false</valido><cuentas>El usuario no existe.</cuentas>></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><cuentas>El usuario no se ha autenticado.</cuentas></respuesta>";
        }
    }
    

    /**
     * Web service operation
     */
    @WebMethod(operationName = "consultarCuenta")
    public String consultarCuenta(@WebParam(name = "token") String token, @WebParam(name = "cuenta") int cuenta) {
        //TODO write your implementation code here:
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Cuenta cuentaObj = cuentaFacade.find(cuenta);
            if (cuentaObj != null) {
                String resultado = "<respuesta><valido>true</valido><mensaje>Valido.</mensaje><estado>";
                resultado += "<convenio>" + cuentaObj.getConvenio().getConvenio() + "</convenio>";
                resultado += "<estadocuenta>" + cuentaObj.getEstado() + "</estadocuenta>";
                resultado += "<saldo>" + cuentaObj.getSaldo().longValue() + "</saldo>";
                resultado += "</estado></respuesta>";
                return resultado;
            } else {
                return "<respuesta><valido>false</valido><mensaje>La cuenta no existe, para el usuario.</mensaje><estado></estado></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>El usuario no se ha autenticado.</mensaje><estado></estado></respuesta>";
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "depositos")
    public String depositos(@WebParam(name = "token") String token, @WebParam(name = "numCuenta") int numCuenta, 
            @WebParam(name = "monto") float monto, @WebParam(name = "descripcion") String descripcion) {
        //TODO write your implementation code here:
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Cuenta cuentaObj = cuentaFacade.find(numCuenta);
            if (cuentaObj != null) {
                //creando la transaccion
                Transaccion trans = new Transaccion(null, new Date(), new BigDecimal(monto), descripcion);
                trans.setIdTipoTransaccion(tipoTransaccionFacade.find(1));
                trans.setIdCuenta(cuentaObj);
                transaccionFacade.create(trans);
                //editando el saldo
                cuentaObj.setSaldo(new BigDecimal(cuentaObj.getSaldo().longValue() + monto));
                cuentaFacade.edit(cuentaObj);
                return "<respuesta><valido>true</valido><mensaje>Valido.</mensaje><transaccion>"+1+"</transaccion></respuesta>";
            } else {
                return "<respuesta><valido>false</valido><mensaje>No existe la cuenta.</mensaje><transaccion></transaccion></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>El usuario no se ha autenticado.</mensaje><transaccion></transaccion></respuesta>";
        }
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "consultaSeguroVida")
    public String consultaSeguroVida(@WebParam(name = "token") String token,@WebParam(name = "codigo") int codigoSeguro) {
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Seguro seguro = seguroFacade.find(codigoSeguro);
            if (seguro != null) {
                String resultado = "<respuesta><valido>false</valido><mensaje>No se ha autenticado.</mensaje><seguro>";
                resultado += "<dinero>" + seguro.getDinero().longValue() + "</dinero>";
                resultado += "<prima>" + seguro.getPrima() + "</prima>";
                resultado += "<cuota>" + seguro.getCuota()+ "</cuota>";
                resultado += "<numcuotas>" + seguro.getNocuotas()+ "</numcuotas>";
                resultado += "<vigencia>" + seguro.getVigencia()+ "</vigencia>";
                resultado += "</seguro></respuesta>";
                return resultado;
            } else {
                return "<respuesta><valido>true</valido><mensaje>No existe el seguro de vida buscado.</mensaje><seguro></seguro></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>No se ha autenticado.</mensaje><seguro></seguro></respuesta>";
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "segurosVidaPorConvenio")
    public String segurosVidaPorConvenio(@WebParam(name = "token") String token, @WebParam(name = "convenio") int convenio) {
        //TODO write your implementation code here:
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Usuario usr = usuarioFacade.find(convenio);
            if (usr != null) {
                //aperturando la nueva cuenta
                List<UsuarioSeguro> lista = usr.getUsuarioSeguroList(); //cuentaFacade.cuentasPorConvenio(convenio);
                if (lista != null && lista.size() > 0) {
                    String respuesta = "<respuesta><valido>false</valido><seguros>";
                    for (UsuarioSeguro o: lista) {
                        respuesta += "<seguro>"+o.getIdUsuarioSeguro()+"</seguro>";
                    }
                    respuesta += "</seguros></respuesta>";
                    return respuesta;
                } else {
                    return "<respuesta><valido>false</valido><seguros>El usuario no posee cuentas.</seguros></respuesta>";
                }
            } else {
                return "<respuesta><valido>false</valido><seguros>El usuario no existe.</seguros></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><seguros>El usuario no se ha autenticado.</seguros></respuesta>";
        }
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "agregarSeguroVida")
    public String agregarSeguroVida(@WebParam(name = "token") String token, @WebParam(name = "convenio") int convenio, 
            @WebParam(name = "codigoSeguro") int codigoSeguro) {
        //TODO write your implementation code here:
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Usuario usr = usuarioFacade.find(convenio);
            if (usr != null) {
                UsuarioSeguro usuarioSeguro = new UsuarioSeguro(null, new Date());
                usuarioSeguro.setConvenio(usr);
                usuarioSeguro.setIdSeguro(new Seguro(1));
                usuarioSeguroFacade.create(usuarioSeguro);
                return "<respuesta><valido>true</valido><mensaje>"+usuarioSeguro.getIdUsuarioSeguro()+"</mensaje></respuesta>";
            } else {
                return "<respuesta><valido>false</valido><mensaje>No existe el convenido especifico.</mensaje></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>No se encuentra autenticado.</mensaje></respuesta>";
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "consultarSeguroDeVidaEspecifico")
    public String consultarSeguroDeVidaEspecifico(@WebParam(name = "token") String token, @WebParam(name = "usuarioseguro") int usuarioseguro) {
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            UsuarioSeguro usuarioSeguro = usuarioSeguroFacade.find(usuarioseguro);
            if (usuarioSeguro != null) {
                //editando el saldo
                String resultado = "<respuesta><valido>true</valido><seguro>";
                resultado += "<dinero>"+usuarioSeguro.getIdSeguro().getDinero()+"</dinero>";
                resultado += "<prima>"+usuarioSeguro.getIdSeguro().getPrima()+"</prima>";
                resultado += "<cuota>"+usuarioSeguro.getIdSeguro().getCuota().longValue()+"</cuota>";
                resultado += "<numcuotas>"+usuarioSeguro.getIdSeguro().getNocuotas()+"</numcuotas>";
                resultado += "<numcuotasactuales>"+usuarioSeguro.getTransaccionList().size()+"</numcuotasactuales>";
                resultado += "<vigencia>"+usuarioSeguro.getIdSeguro().getVigencia()+"</vigencia>";
                resultado += "</seguro></respuesta>";
                return resultado;
            } else {
                return "<respuesta><valido>false</valido><mensaje>La cuenta no existe.</mensaje></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>No se encuentra autenticado.</mensaje></respuesta>";
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "pagarSeguroVida")
    public String pagarSeguroVida(@WebParam(name = "token") String token, @WebParam(name = "usuarioseguro") int usuarioseguro, 
            @WebParam(name = "monto") long monto, @WebParam(name = "descripcion") String descripcion) {
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            UsuarioSeguro usuarioSeguro = usuarioSeguroFacade.find(usuarioseguro);
            if (usuarioSeguro != null) {
                //creando la transaccion
                Transaccion trans = new Transaccion(null, new Date(), new BigDecimal(monto), descripcion);
                trans.setIdTipoTransaccion(tipoTransaccionFacade.find(3));
                trans.setIdUsuarioSeguro(usuarioSeguro);
                transaccionFacade.create(trans);
                //editando el saldo
                return "<respuesta><valido>true</valido><mensaje>"+trans.getIdTransaccion()+"</mensaje></respuesta>";
            } else {
                return "<respuesta><valido>false</valido><mensaje>La cuenta no existe.</mensaje></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>No se encuentra autenticado.</mensaje></respuesta>";
        }
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "realizarPrestamoAutomovil")
    public String realizarPrestamoAutomovil(@WebParam(name = "token") String token, @WebParam(name = "convenio") int convenio,
            @WebParam(name = "modelo") short modelo, @WebParam(name = "placa") String placa, 
            @WebParam(name = "chasis") String chasis, @WebParam(name = "monto") long monto, 
            @WebParam(name = "numPagos") int numPagos, @WebParam(name = "interes") short interes) {
        //TODO write your implementation code here:
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Usuario usr = usuarioFacade.find(convenio);
            if (usr != null) {
                PrestamoAuto prestamosAuto = new PrestamoAuto(null, modelo, placa, chasis, new BigDecimal(monto), numPagos, interes, (short)1);
                prestamosAuto.setConvenio(usr);
                prestamoAutoFacade.create(prestamosAuto);
                return "<respuesta><valido>true</valido><mensaje>Prestamo creado con exito.</mensaje></respuesta>";
            } else {
                return "<respuesta><valido>false</valido><mensaje>El usuario es invalido.</mensaje></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>No se encuentra autenticado.</mensaje></respuesta>";
        }
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "prestamoAutomovilPorConvenio")
    public String prestamoAutomovilPorConvenio(@WebParam(name = "token") String token, @WebParam(name = "convenio") int convenio) {
        //TODO write your implementation code here:
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Usuario usr = usuarioFacade.find(convenio);
            if (usr != null) {
                //aperturando la nueva cuenta
                List<PrestamoAuto> lista = usr.getPrestamoAutoList(); //cuentaFacade.cuentasPorConvenio(convenio);
                if (lista != null && lista.size() > 0) {
                    String respuesta = "<respuesta><valido>false</valido><prestamos>";
                    for (PrestamoAuto o: lista) {
                        respuesta += "<prestamo>"+o.getIdPrestamoAuto()+"</prestamo>";
                    }
                    respuesta += "</prestamos></respuesta>";
                    return respuesta;
                } else {
                    return "<respuesta><valido>false</valido><prestamos>El usuario no posee Prestamos de automovil.</prestamos></respuesta>";
                }
            } else {
                return "<respuesta><valido>false</valido><prestamos>El usuario no existe.</prestamos></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><prestamos>El usuario no se ha autenticado.</prestamos></respuesta>";
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "consultarPrestamoAutomovil")
    public String consultarPrestamoAutomovil(@WebParam(name = "token") String token, 
            @WebParam(name = "precioBase") long precioBase, 
            @WebParam(name = "modelo") short modelo) {
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Usuario usr = usuarioFacade.find(beanControlAuth.getUsuarioLogueado(token).getConvenio());
            if (usr != null) {
                List<PrestamoAutoCatalogo> prestamos = prestamoAutoCatalogoFacade.findBetweenDataRange(precioBase, modelo);
                long monto = 0;
                if (prestamos.size() > 0) {
                    monto = (prestamos.get(0).getPorcentaje()*precioBase)/100;
                }
                return "<respuesta><valido>true</valido><mensaje>Valido.</mensaje><monto>"+monto+"</monto></respuesta>";
            } else {
                return "<respuesta><valido>false</valido><mensaje>Usuario invalido.</mensaje><monto></monto></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>No se ha autenticado.</mensaje><monto></monto></respuesta>";
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "consultarHipotecaDelCatalogo")
    public String consultarHipotecaDelCatalogo(@WebParam(name = "token") String token, 
            @WebParam(name = "area") long area, @WebParam(name = "precioBase") long precioBase) {
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Usuario usr = usuarioFacade.find(beanControlAuth.getUsuarioLogueado(token).getConvenio());
            if (usr != null) {
                List<PrestamoHipotecaCatalogo> prestamos = prestamoHipotecaCatalogoFacade.findBetweenDataRange(precioBase, area);
                long monto = 0;
                if (prestamos.size() > 0) {
                    monto = (prestamos.get(0).getPorcentaje()*precioBase)/100;
                }
                return "<respuesta><valido>true</valido><mensaje>Valido.</mensaje><monto>"+monto+"</monto></respuesta>";
            } else {
                return "<respuesta><valido>false</valido><mensaje>Usuario invalido.</mensaje><monto></monto></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>No se ha autenticado.</mensaje><monto></monto></respuesta>";
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "realizarPrestamoHipotecario")
    public String realizarPrestamoHipotecario(@WebParam(name = "token") String token, @WebParam(name = "convenio") int convenio,
            @WebParam(name = "descripcion") String descripcion, @WebParam(name = "area") int area, 
            @WebParam(name = "direccion") String direccion, @WebParam(name = "monto") long monto, 
            @WebParam(name = "numPagos") int numPagos, @WebParam(name = "interes") short interes) {
        //TODO write your implementation code here:
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Usuario usr = usuarioFacade.find(convenio);
            if (usr != null) {
                PrestamoHipoteca prestamoHipoteca = new PrestamoHipoteca(null, descripcion, monto, direccion, new BigDecimal(monto), numPagos, interes, (short)1);
                prestamoHipoteca.setConvenio(usr);
                prestamoHipotecaFacade.create(prestamoHipoteca);
                return "<respuesta><valido>true</valido><mensaje>Valido.</mensaje><monto>"+monto+"</monto></respuesta>";
            } else {
                return "<respuesta><valido>false</valido><mensaje>Usuario invalido.</mensaje><monto></monto></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><mensaje>No se ha autenticado.</mensaje><monto></monto></respuesta>";
        }
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "prestamoHipotecarioPorConvenio")
    public String prestamoHipotecarioPorConvenio(@WebParam(name = "token") String token, @WebParam(name = "convenio") int convenio) {
        //TODO write your implementation code here:
        if (beanControlAuth.getUsuarioLogueado(token) != null) {
            Usuario usr = usuarioFacade.find(convenio);
            if (usr != null) {
                //aperturando la nueva cuenta
                List<PrestamoHipoteca> lista = usr.getPrestamoHipotecaList(); //cuentaFacade.cuentasPorConvenio(convenio);
                if (lista != null && lista.size() > 0) {
                    String respuesta = "<respuesta><valido>false</valido><prestamos>";
                    for (PrestamoHipoteca o: lista) {
                        respuesta += "<prestamo>"+o.getIdPrestamoHipoteca()+"</prestamo>";
                    }
                    respuesta += "</prestamos></respuesta>";
                    return respuesta;
                } else {
                    return "<respuesta><valido>false</valido><prestamos>El usuario no posee Hipotecas.</prestamos>></respuesta>";
                }
            } else {
                return "<respuesta><valido>false</valido><prestamos>El usuario no existe.</prestamos></respuesta>";
            }
        } else {
            return "<respuesta><valido>false</valido><prestamos>El usuario no se ha autenticado.</prestamos></respuesta>";
        }
    }
}
