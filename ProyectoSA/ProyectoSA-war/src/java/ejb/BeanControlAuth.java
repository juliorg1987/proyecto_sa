/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

/**
 *
 * @author victor
 */
import java.util.GregorianCalendar;
import java.util.HashMap;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import proyecto.sa.util.UsuarioAuth;
@Singleton
@LocalBean
public class BeanControlAuth {

    private final HashMap<String, UsuarioAuth> usuarios = new HashMap<String, UsuarioAuth>();

    public UsuarioAuth getUsuarioLogueado(String token) {
        if (usuarios.containsKey(token)) {
            return usuarios.get(token);
        }
        return null;
    }
    
    public String putUsuarioLogueado(Usuario u){
        String token = GregorianCalendar.getInstance().getTimeInMillis()+"";
        usuarios.put(token, new UsuarioAuth(u.getConvenio(), u.getApellido1()+ ", " + u.getNombre1(), null, token));
        return token;
    }

    public HashMap<String, UsuarioAuth> getUsuarios() {
        return usuarios;
    }
    
    
    
    
}
