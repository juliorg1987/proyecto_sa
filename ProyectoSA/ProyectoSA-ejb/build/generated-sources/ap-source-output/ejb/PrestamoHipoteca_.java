package ejb;

import ejb.Transaccion;
import ejb.Usuario;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(PrestamoHipoteca.class)
public class PrestamoHipoteca_ { 

    public static volatile SingularAttribute<PrestamoHipoteca, String> direccion;
    public static volatile SingularAttribute<PrestamoHipoteca, Long> area;
    public static volatile SingularAttribute<PrestamoHipoteca, Short> estado;
    public static volatile SingularAttribute<PrestamoHipoteca, Usuario> convenio;
    public static volatile SingularAttribute<PrestamoHipoteca, Integer> numPagos;
    public static volatile SingularAttribute<PrestamoHipoteca, String> descripcion;
    public static volatile ListAttribute<PrestamoHipoteca, Transaccion> transaccionList;
    public static volatile SingularAttribute<PrestamoHipoteca, Integer> idPrestamoHipoteca;
    public static volatile SingularAttribute<PrestamoHipoteca, BigDecimal> monto;
    public static volatile SingularAttribute<PrestamoHipoteca, Short> interes;

}