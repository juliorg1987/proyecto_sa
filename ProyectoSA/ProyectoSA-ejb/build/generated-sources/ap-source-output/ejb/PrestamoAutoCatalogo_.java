package ejb;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(PrestamoAutoCatalogo.class)
public class PrestamoAutoCatalogo_ { 

    public static volatile SingularAttribute<PrestamoAutoCatalogo, Short> aniominimo;
    public static volatile SingularAttribute<PrestamoAutoCatalogo, Short> porcentaje;
    public static volatile SingularAttribute<PrestamoAutoCatalogo, Integer> idPrestamoAutoCatalogo;
    public static volatile SingularAttribute<PrestamoAutoCatalogo, Short> aniomaximo;
    public static volatile SingularAttribute<PrestamoAutoCatalogo, String> descripcion;
    public static volatile SingularAttribute<PrestamoAutoCatalogo, BigDecimal> preciobasemin;
    public static volatile SingularAttribute<PrestamoAutoCatalogo, BigDecimal> preciobasemax;

}