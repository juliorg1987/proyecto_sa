package ejb;

import ejb.Transaccion;
import ejb.Usuario;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(PrestamoAuto.class)
public class PrestamoAuto_ { 

    public static volatile SingularAttribute<PrestamoAuto, Short> estado;
    public static volatile SingularAttribute<PrestamoAuto, Usuario> convenio;
    public static volatile SingularAttribute<PrestamoAuto, Integer> idPrestamoAuto;
    public static volatile SingularAttribute<PrestamoAuto, Integer> numPagos;
    public static volatile SingularAttribute<PrestamoAuto, String> placa;
    public static volatile SingularAttribute<PrestamoAuto, String> chasis;
    public static volatile ListAttribute<PrestamoAuto, Transaccion> transaccionList;
    public static volatile SingularAttribute<PrestamoAuto, Short> modelo;
    public static volatile SingularAttribute<PrestamoAuto, BigDecimal> monto;
    public static volatile SingularAttribute<PrestamoAuto, Short> interes;

}