package ejb;

import ejb.TipoSeguro;
import ejb.UsuarioSeguro;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(Seguro.class)
public class Seguro_ { 

    public static volatile ListAttribute<Seguro, UsuarioSeguro> usuarioSeguroList;
    public static volatile SingularAttribute<Seguro, Integer> nocuotas;
    public static volatile SingularAttribute<Seguro, Short> estado;
    public static volatile SingularAttribute<Seguro, Integer> prima;
    public static volatile SingularAttribute<Seguro, Integer> idSeguro;
    public static volatile SingularAttribute<Seguro, TipoSeguro> idTipoSeguro;
    public static volatile SingularAttribute<Seguro, Integer> vigencia;
    public static volatile SingularAttribute<Seguro, BigDecimal> cuota;
    public static volatile SingularAttribute<Seguro, BigDecimal> dinero;
    public static volatile SingularAttribute<Seguro, String> beneficiario;

}