package ejb;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(PrestamoHipotecaCatalogo.class)
public class PrestamoHipotecaCatalogo_ { 

    public static volatile SingularAttribute<PrestamoHipotecaCatalogo, Short> aniominimo;
    public static volatile SingularAttribute<PrestamoHipotecaCatalogo, Short> porcentaje;
    public static volatile SingularAttribute<PrestamoHipotecaCatalogo, Short> aniomaximo;
    public static volatile SingularAttribute<PrestamoHipotecaCatalogo, String> descripcion;
    public static volatile SingularAttribute<PrestamoHipotecaCatalogo, BigDecimal> preciobasemin;
    public static volatile SingularAttribute<PrestamoHipotecaCatalogo, BigDecimal> preciobasemax;
    public static volatile SingularAttribute<PrestamoHipotecaCatalogo, Integer> idPrestamoHCatalogo;

}