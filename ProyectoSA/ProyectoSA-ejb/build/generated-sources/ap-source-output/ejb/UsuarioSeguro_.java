package ejb;

import ejb.Seguro;
import ejb.Transaccion;
import ejb.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(UsuarioSeguro.class)
public class UsuarioSeguro_ { 

    public static volatile SingularAttribute<UsuarioSeguro, Date> fecha;
    public static volatile SingularAttribute<UsuarioSeguro, Usuario> convenio;
    public static volatile SingularAttribute<UsuarioSeguro, Integer> idUsuarioSeguro;
    public static volatile SingularAttribute<UsuarioSeguro, Seguro> idSeguro;
    public static volatile ListAttribute<UsuarioSeguro, Transaccion> transaccionList;

}