package ejb;

import ejb.Transaccion;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(TipoTransaccion.class)
public class TipoTransaccion_ { 

    public static volatile SingularAttribute<TipoTransaccion, String> nombre;
    public static volatile SingularAttribute<TipoTransaccion, Integer> idTipoTransaccion;
    public static volatile SingularAttribute<TipoTransaccion, String> descripcion;
    public static volatile ListAttribute<TipoTransaccion, Transaccion> transaccionList;

}