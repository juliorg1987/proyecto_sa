package ejb;

import ejb.Cuenta;
import ejb.PrestamoAuto;
import ejb.PrestamoHipoteca;
import ejb.UsuarioSeguro;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, String> apellido2;
    public static volatile SingularAttribute<Usuario, String> passwd;
    public static volatile ListAttribute<Usuario, UsuarioSeguro> usuarioSeguroList;
    public static volatile SingularAttribute<Usuario, Short> estado;
    public static volatile SingularAttribute<Usuario, String> nombre2;
    public static volatile SingularAttribute<Usuario, Integer> convenio;
    public static volatile SingularAttribute<Usuario, String> nombre1;
    public static volatile ListAttribute<Usuario, PrestamoAuto> prestamoAutoList;
    public static volatile SingularAttribute<Usuario, String> apellido1;
    public static volatile ListAttribute<Usuario, PrestamoHipoteca> prestamoHipotecaList;
    public static volatile SingularAttribute<Usuario, Long> dpi;
    public static volatile ListAttribute<Usuario, Cuenta> cuentaList;

}