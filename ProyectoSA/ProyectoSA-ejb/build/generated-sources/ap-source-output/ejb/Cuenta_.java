package ejb;

import ejb.Transaccion;
import ejb.Usuario;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(Cuenta.class)
public class Cuenta_ { 

    public static volatile SingularAttribute<Cuenta, String> nombre;
    public static volatile SingularAttribute<Cuenta, String> direccion;
    public static volatile SingularAttribute<Cuenta, Short> estado;
    public static volatile SingularAttribute<Cuenta, Usuario> convenio;
    public static volatile SingularAttribute<Cuenta, String> telefono;
    public static volatile ListAttribute<Cuenta, Transaccion> transaccionList;
    public static volatile SingularAttribute<Cuenta, Long> dpi;
    public static volatile SingularAttribute<Cuenta, Integer> idCuenta;
    public static volatile SingularAttribute<Cuenta, BigDecimal> saldo;

}