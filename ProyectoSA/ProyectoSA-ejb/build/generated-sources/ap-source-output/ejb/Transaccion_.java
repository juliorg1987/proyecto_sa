package ejb;

import ejb.Cuenta;
import ejb.PrestamoAuto;
import ejb.PrestamoHipoteca;
import ejb.TipoTransaccion;
import ejb.UsuarioSeguro;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(Transaccion.class)
public class Transaccion_ { 

    public static volatile SingularAttribute<Transaccion, Date> fecha;
    public static volatile SingularAttribute<Transaccion, UsuarioSeguro> idUsuarioSeguro;
    public static volatile SingularAttribute<Transaccion, TipoTransaccion> idTipoTransaccion;
    public static volatile SingularAttribute<Transaccion, PrestamoAuto> idPrestamoAuto;
    public static volatile SingularAttribute<Transaccion, Integer> idTransaccion;
    public static volatile SingularAttribute<Transaccion, String> descripcion;
    public static volatile SingularAttribute<Transaccion, PrestamoHipoteca> idPrestamoHipoteca;
    public static volatile SingularAttribute<Transaccion, Cuenta> idCuenta;
    public static volatile SingularAttribute<Transaccion, BigDecimal> monto;

}