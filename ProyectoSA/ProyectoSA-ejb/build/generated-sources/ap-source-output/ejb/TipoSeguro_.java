package ejb;

import ejb.Seguro;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-05-14T11:20:47")
@StaticMetamodel(TipoSeguro.class)
public class TipoSeguro_ { 

    public static volatile SingularAttribute<TipoSeguro, String> nombre;
    public static volatile ListAttribute<TipoSeguro, Seguro> seguroList;
    public static volatile SingularAttribute<TipoSeguro, Integer> idTipoSeguro;
    public static volatile SingularAttribute<TipoSeguro, String> descripcion;

}