/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author victor
 */
@Local
public interface UsuarioSeguroFacadeLocal {

    void create(UsuarioSeguro usuarioSeguro);

    void edit(UsuarioSeguro usuarioSeguro);

    void remove(UsuarioSeguro usuarioSeguro);

    UsuarioSeguro find(Object id);

    List<UsuarioSeguro> findAll();

    List<UsuarioSeguro> findRange(int[] range);

    int count();
    
}
