/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "PrestamoHipoteca")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PrestamoHipoteca.findAll", query = "SELECT p FROM PrestamoHipoteca p")})
public class PrestamoHipoteca implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPrestamoHipoteca")
    private Integer idPrestamoHipoteca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "area")
    private long area;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "direccion")
    private String direccion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto")
    private BigDecimal monto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numPagos")
    private int numPagos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "interes")
    private short interes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private short estado;
    @JoinColumn(name = "convenio", referencedColumnName = "convenio")
    @ManyToOne(optional = false)
    private Usuario convenio;
    @OneToMany(mappedBy = "idPrestamoHipoteca")
    private List<Transaccion> transaccionList;

    public PrestamoHipoteca() {
    }

    public PrestamoHipoteca(Integer idPrestamoHipoteca) {
        this.idPrestamoHipoteca = idPrestamoHipoteca;
    }

    public PrestamoHipoteca(Integer idPrestamoHipoteca, String descripcion, long area, String direccion, BigDecimal monto, int numPagos, short interes, short estado) {
        this.idPrestamoHipoteca = idPrestamoHipoteca;
        this.descripcion = descripcion;
        this.area = area;
        this.direccion = direccion;
        this.monto = monto;
        this.numPagos = numPagos;
        this.interes = interes;
        this.estado = estado;
    }

    public Integer getIdPrestamoHipoteca() {
        return idPrestamoHipoteca;
    }

    public void setIdPrestamoHipoteca(Integer idPrestamoHipoteca) {
        this.idPrestamoHipoteca = idPrestamoHipoteca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getArea() {
        return area;
    }

    public void setArea(long area) {
        this.area = area;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public int getNumPagos() {
        return numPagos;
    }

    public void setNumPagos(int numPagos) {
        this.numPagos = numPagos;
    }

    public short getInteres() {
        return interes;
    }

    public void setInteres(short interes) {
        this.interes = interes;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    public Usuario getConvenio() {
        return convenio;
    }

    public void setConvenio(Usuario convenio) {
        this.convenio = convenio;
    }

    @XmlTransient
    public List<Transaccion> getTransaccionList() {
        return transaccionList;
    }

    public void setTransaccionList(List<Transaccion> transaccionList) {
        this.transaccionList = transaccionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPrestamoHipoteca != null ? idPrestamoHipoteca.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrestamoHipoteca)) {
            return false;
        }
        PrestamoHipoteca other = (PrestamoHipoteca) object;
        if ((this.idPrestamoHipoteca == null && other.idPrestamoHipoteca != null) || (this.idPrestamoHipoteca != null && !this.idPrestamoHipoteca.equals(other.idPrestamoHipoteca))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.PrestamoHipoteca[ idPrestamoHipoteca=" + idPrestamoHipoteca + " ]";
    }
    
}
