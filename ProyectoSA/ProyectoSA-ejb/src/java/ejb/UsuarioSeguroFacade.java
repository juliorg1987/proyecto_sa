/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author victor
 */
@Stateless
public class UsuarioSeguroFacade extends AbstractFacade<UsuarioSeguro> implements UsuarioSeguroFacadeLocal {
    @PersistenceContext(unitName = "ProyectoSA-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioSeguroFacade() {
        super(UsuarioSeguro.class);
    }
    
}
