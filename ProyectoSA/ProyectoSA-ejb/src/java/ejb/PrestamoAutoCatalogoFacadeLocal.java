/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author victor
 */
@Local
public interface PrestamoAutoCatalogoFacadeLocal {

    void create(PrestamoAutoCatalogo prestamoAutoCatalogo);

    void edit(PrestamoAutoCatalogo prestamoAutoCatalogo);

    void remove(PrestamoAutoCatalogo prestamoAutoCatalogo);

    PrestamoAutoCatalogo find(Object id);

    List<PrestamoAutoCatalogo> findAll();
    
    List<PrestamoAutoCatalogo> findBetweenDataRange(long preciobase, short modelo);

    List<PrestamoAutoCatalogo> findRange(int[] range);

    int count();
    
}
