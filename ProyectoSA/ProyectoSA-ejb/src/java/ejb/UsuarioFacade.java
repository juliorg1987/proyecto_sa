/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author victor
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {
    @PersistenceContext(unitName = "ProyectoSA-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    @Override
    public Usuario findByDPI(long dpi){
        Query query = getEntityManager().createQuery("SELECT u FROM Usuario u WHERE u.dpi = :dpi_param");
        query.setParameter("dpi_param", dpi);
        List<Usuario> list = query.getResultList();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public Usuario auntenticar(int convenio, String password) {
        Query query = getEntityManager().createQuery("SELECT u FROM Usuario u WHERE u.convenio = :convenio_param AND u.passwd = :passwd_param");
        query.setParameter("convenio_param", convenio);
        query.setParameter("passwd_param", password);
        List<Usuario> list = query.getResultList();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

}
