/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author victor
 */
@Local
public interface PrestamoHipotecaCatalogoFacadeLocal {

    void create(PrestamoHipotecaCatalogo prestamoHipotecaCatalogo);

    void edit(PrestamoHipotecaCatalogo prestamoHipotecaCatalogo);

    void remove(PrestamoHipotecaCatalogo prestamoHipotecaCatalogo);

    PrestamoHipotecaCatalogo find(Object id);

    List<PrestamoHipotecaCatalogo> findAll();
    
    List<PrestamoHipotecaCatalogo> findBetweenDataRange(long preciobase, long area);

    List<PrestamoHipotecaCatalogo> findRange(int[] range);

    int count();
    
}
