/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "PrestamoAutoCatalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PrestamoAutoCatalogo.findAll", query = "SELECT p FROM PrestamoAutoCatalogo p")})
public class PrestamoAutoCatalogo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPrestamoAutoCatalogo")
    private Integer idPrestamoAutoCatalogo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "aniominimo")
    private short aniominimo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "aniomaximo")
    private short aniomaximo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "preciobasemin")
    private BigDecimal preciobasemin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "preciobasemax")
    private BigDecimal preciobasemax;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje")
    private short porcentaje;

    public PrestamoAutoCatalogo() {
    }

    public PrestamoAutoCatalogo(Integer idPrestamoAutoCatalogo) {
        this.idPrestamoAutoCatalogo = idPrestamoAutoCatalogo;
    }

    public PrestamoAutoCatalogo(Integer idPrestamoAutoCatalogo, String descripcion, short aniominimo, short aniomaximo, BigDecimal preciobasemin, BigDecimal preciobasemax, short porcentaje) {
        this.idPrestamoAutoCatalogo = idPrestamoAutoCatalogo;
        this.descripcion = descripcion;
        this.aniominimo = aniominimo;
        this.aniomaximo = aniomaximo;
        this.preciobasemin = preciobasemin;
        this.preciobasemax = preciobasemax;
        this.porcentaje = porcentaje;
    }

    public Integer getIdPrestamoAutoCatalogo() {
        return idPrestamoAutoCatalogo;
    }

    public void setIdPrestamoAutoCatalogo(Integer idPrestamoAutoCatalogo) {
        this.idPrestamoAutoCatalogo = idPrestamoAutoCatalogo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public short getAniominimo() {
        return aniominimo;
    }

    public void setAniominimo(short aniominimo) {
        this.aniominimo = aniominimo;
    }

    public short getAniomaximo() {
        return aniomaximo;
    }

    public void setAniomaximo(short aniomaximo) {
        this.aniomaximo = aniomaximo;
    }

    public BigDecimal getPreciobasemin() {
        return preciobasemin;
    }

    public void setPreciobasemin(BigDecimal preciobasemin) {
        this.preciobasemin = preciobasemin;
    }

    public BigDecimal getPreciobasemax() {
        return preciobasemax;
    }

    public void setPreciobasemax(BigDecimal preciobasemax) {
        this.preciobasemax = preciobasemax;
    }

    public short getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(short porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPrestamoAutoCatalogo != null ? idPrestamoAutoCatalogo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrestamoAutoCatalogo)) {
            return false;
        }
        PrestamoAutoCatalogo other = (PrestamoAutoCatalogo) object;
        if ((this.idPrestamoAutoCatalogo == null && other.idPrestamoAutoCatalogo != null) || (this.idPrestamoAutoCatalogo != null && !this.idPrestamoAutoCatalogo.equals(other.idPrestamoAutoCatalogo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.PrestamoAutoCatalogo[ idPrestamoAutoCatalogo=" + idPrestamoAutoCatalogo + " ]";
    }
    
}
