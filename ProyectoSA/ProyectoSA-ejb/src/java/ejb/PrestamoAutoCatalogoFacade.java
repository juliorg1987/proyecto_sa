/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author victor
 */
@Stateless
public class PrestamoAutoCatalogoFacade extends AbstractFacade<PrestamoAutoCatalogo> implements PrestamoAutoCatalogoFacadeLocal {
    @PersistenceContext(unitName = "ProyectoSA-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrestamoAutoCatalogoFacade() {
        super(PrestamoAutoCatalogo.class);
    }

    @Override
    public List<PrestamoAutoCatalogo> findBetweenDataRange(long preciobase, short modelo) {
        Query query = getEntityManager().createQuery("SELECT p FROM PrestamoAutoCatalogo p WHERE :modelo1 >= p.aniominimo AND :modelo2 <= p.aniomaximo AND :preciobase1 >= p.preciobasemin AND :preciobase2 <= p.preciobasemax");
        query.setParameter("modelo1", modelo);
        query.setParameter("modelo2", modelo);
        query.setParameter("preciobase1", preciobase);
        query.setParameter("preciobase2", preciobase);
        return query.getResultList();
    }
    
}
