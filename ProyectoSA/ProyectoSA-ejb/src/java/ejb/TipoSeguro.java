/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "TipoSeguro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoSeguro.findAll", query = "SELECT t FROM TipoSeguro t")})
public class TipoSeguro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoSeguro")
    private Integer idTipoSeguro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2048)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoSeguro")
    private List<Seguro> seguroList;

    public TipoSeguro() {
    }

    public TipoSeguro(Integer idTipoSeguro) {
        this.idTipoSeguro = idTipoSeguro;
    }

    public TipoSeguro(Integer idTipoSeguro, String nombre) {
        this.idTipoSeguro = idTipoSeguro;
        this.nombre = nombre;
    }

    public Integer getIdTipoSeguro() {
        return idTipoSeguro;
    }

    public void setIdTipoSeguro(Integer idTipoSeguro) {
        this.idTipoSeguro = idTipoSeguro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Seguro> getSeguroList() {
        return seguroList;
    }

    public void setSeguroList(List<Seguro> seguroList) {
        this.seguroList = seguroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoSeguro != null ? idTipoSeguro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoSeguro)) {
            return false;
        }
        TipoSeguro other = (TipoSeguro) object;
        if ((this.idTipoSeguro == null && other.idTipoSeguro != null) || (this.idTipoSeguro != null && !this.idTipoSeguro.equals(other.idTipoSeguro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.TipoSeguro[ idTipoSeguro=" + idTipoSeguro + " ]";
    }
    
}
