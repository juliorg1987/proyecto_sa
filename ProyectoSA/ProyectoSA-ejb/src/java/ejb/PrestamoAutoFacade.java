/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author victor
 */
@Stateless
public class PrestamoAutoFacade extends AbstractFacade<PrestamoAuto> implements PrestamoAutoFacadeLocal {
    @PersistenceContext(unitName = "ProyectoSA-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrestamoAutoFacade() {
        super(PrestamoAuto.class);
    }
    
}
