/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "Seguro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Seguro.findAll", query = "SELECT s FROM Seguro s")})
public class Seguro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idSeguro")
    private Integer idSeguro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prima")
    private int prima;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "cuota")
    private BigDecimal cuota;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nocuotas")
    private int nocuotas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vigencia")
    private int vigencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dinero")
    private BigDecimal dinero;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "beneficiario")
    private String beneficiario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private short estado;
    @JoinColumn(name = "idTipoSeguro", referencedColumnName = "idTipoSeguro")
    @ManyToOne(optional = false)
    private TipoSeguro idTipoSeguro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSeguro")
    private List<UsuarioSeguro> usuarioSeguroList;

    public Seguro() {
    }

    public Seguro(Integer idSeguro) {
        this.idSeguro = idSeguro;
    }

    public Seguro(Integer idSeguro, int prima, BigDecimal cuota, int nocuotas, int vigencia, BigDecimal dinero, String beneficiario, short estado) {
        this.idSeguro = idSeguro;
        this.prima = prima;
        this.cuota = cuota;
        this.nocuotas = nocuotas;
        this.vigencia = vigencia;
        this.dinero = dinero;
        this.beneficiario = beneficiario;
        this.estado = estado;
    }

    public Integer getIdSeguro() {
        return idSeguro;
    }

    public void setIdSeguro(Integer idSeguro) {
        this.idSeguro = idSeguro;
    }

    public int getPrima() {
        return prima;
    }

    public void setPrima(int prima) {
        this.prima = prima;
    }

    public BigDecimal getCuota() {
        return cuota;
    }

    public void setCuota(BigDecimal cuota) {
        this.cuota = cuota;
    }

    public int getNocuotas() {
        return nocuotas;
    }

    public void setNocuotas(int nocuotas) {
        this.nocuotas = nocuotas;
    }

    public int getVigencia() {
        return vigencia;
    }

    public void setVigencia(int vigencia) {
        this.vigencia = vigencia;
    }

    public BigDecimal getDinero() {
        return dinero;
    }

    public void setDinero(BigDecimal dinero) {
        this.dinero = dinero;
    }

    public String getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    public TipoSeguro getIdTipoSeguro() {
        return idTipoSeguro;
    }

    public void setIdTipoSeguro(TipoSeguro idTipoSeguro) {
        this.idTipoSeguro = idTipoSeguro;
    }

    @XmlTransient
    public List<UsuarioSeguro> getUsuarioSeguroList() {
        return usuarioSeguroList;
    }

    public void setUsuarioSeguroList(List<UsuarioSeguro> usuarioSeguroList) {
        this.usuarioSeguroList = usuarioSeguroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSeguro != null ? idSeguro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Seguro)) {
            return false;
        }
        Seguro other = (Seguro) object;
        if ((this.idSeguro == null && other.idSeguro != null) || (this.idSeguro != null && !this.idSeguro.equals(other.idSeguro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.Seguro[ idSeguro=" + idSeguro + " ]";
    }
    
}
