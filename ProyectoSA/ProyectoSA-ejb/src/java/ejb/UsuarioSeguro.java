/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "UsuarioSeguro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioSeguro.findAll", query = "SELECT u FROM UsuarioSeguro u")})
public class UsuarioSeguro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idUsuarioSeguro")
    private Integer idUsuarioSeguro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @OneToMany(mappedBy = "idUsuarioSeguro")
    private List<Transaccion> transaccionList;
    @JoinColumn(name = "convenio", referencedColumnName = "convenio")
    @ManyToOne(optional = false)
    private Usuario convenio;
    @JoinColumn(name = "idSeguro", referencedColumnName = "idSeguro")
    @ManyToOne(optional = false)
    private Seguro idSeguro;

    public UsuarioSeguro() {
    }

    public UsuarioSeguro(Integer idUsuarioSeguro) {
        this.idUsuarioSeguro = idUsuarioSeguro;
    }

    public UsuarioSeguro(Integer idUsuarioSeguro, Date fecha) {
        this.idUsuarioSeguro = idUsuarioSeguro;
        this.fecha = fecha;
    }

    public Integer getIdUsuarioSeguro() {
        return idUsuarioSeguro;
    }

    public void setIdUsuarioSeguro(Integer idUsuarioSeguro) {
        this.idUsuarioSeguro = idUsuarioSeguro;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @XmlTransient
    public List<Transaccion> getTransaccionList() {
        return transaccionList;
    }

    public void setTransaccionList(List<Transaccion> transaccionList) {
        this.transaccionList = transaccionList;
    }

    public Usuario getConvenio() {
        return convenio;
    }

    public void setConvenio(Usuario convenio) {
        this.convenio = convenio;
    }

    public Seguro getIdSeguro() {
        return idSeguro;
    }

    public void setIdSeguro(Seguro idSeguro) {
        this.idSeguro = idSeguro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuarioSeguro != null ? idUsuarioSeguro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioSeguro)) {
            return false;
        }
        UsuarioSeguro other = (UsuarioSeguro) object;
        if ((this.idUsuarioSeguro == null && other.idUsuarioSeguro != null) || (this.idUsuarioSeguro != null && !this.idUsuarioSeguro.equals(other.idUsuarioSeguro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.UsuarioSeguro[ idUsuarioSeguro=" + idUsuarioSeguro + " ]";
    }
    
}
