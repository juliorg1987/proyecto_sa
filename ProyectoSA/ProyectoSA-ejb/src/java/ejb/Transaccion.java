/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "Transaccion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transaccion.findAll", query = "SELECT t FROM Transaccion t")})
public class Transaccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTransaccion")
    private Integer idTransaccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto")
    private BigDecimal monto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2048)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "idUsuarioSeguro", referencedColumnName = "idUsuarioSeguro")
    @ManyToOne
    private UsuarioSeguro idUsuarioSeguro;
    @JoinColumn(name = "idTipoTransaccion", referencedColumnName = "idTipoTransaccion")
    @ManyToOne(optional = false)
    private TipoTransaccion idTipoTransaccion;
    @JoinColumn(name = "idPrestamoAuto", referencedColumnName = "idPrestamoAuto")
    @ManyToOne
    private PrestamoAuto idPrestamoAuto;
    @JoinColumn(name = "idPrestamoHipoteca", referencedColumnName = "idPrestamoHipoteca")
    @ManyToOne
    private PrestamoHipoteca idPrestamoHipoteca;
    @JoinColumn(name = "idCuenta", referencedColumnName = "idCuenta")
    @ManyToOne
    private Cuenta idCuenta;

    public Transaccion() {
    }

    public Transaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Transaccion(Integer idTransaccion, Date fecha, BigDecimal monto, String descripcion) {
        this.idTransaccion = idTransaccion;
        this.fecha = fecha;
        this.monto = monto;
        this.descripcion = descripcion;
    }

    public Integer getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public UsuarioSeguro getIdUsuarioSeguro() {
        return idUsuarioSeguro;
    }

    public void setIdUsuarioSeguro(UsuarioSeguro idUsuarioSeguro) {
        this.idUsuarioSeguro = idUsuarioSeguro;
    }

    public TipoTransaccion getIdTipoTransaccion() {
        return idTipoTransaccion;
    }

    public void setIdTipoTransaccion(TipoTransaccion idTipoTransaccion) {
        this.idTipoTransaccion = idTipoTransaccion;
    }

    public PrestamoAuto getIdPrestamoAuto() {
        return idPrestamoAuto;
    }

    public void setIdPrestamoAuto(PrestamoAuto idPrestamoAuto) {
        this.idPrestamoAuto = idPrestamoAuto;
    }

    public PrestamoHipoteca getIdPrestamoHipoteca() {
        return idPrestamoHipoteca;
    }

    public void setIdPrestamoHipoteca(PrestamoHipoteca idPrestamoHipoteca) {
        this.idPrestamoHipoteca = idPrestamoHipoteca;
    }

    public Cuenta getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Cuenta idCuenta) {
        this.idCuenta = idCuenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTransaccion != null ? idTransaccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transaccion)) {
            return false;
        }
        Transaccion other = (Transaccion) object;
        if ((this.idTransaccion == null && other.idTransaccion != null) || (this.idTransaccion != null && !this.idTransaccion.equals(other.idTransaccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.Transaccion[ idTransaccion=" + idTransaccion + " ]";
    }
    
}
