/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author victor
 */
@Local
public interface PrestamoAutoFacadeLocal {

    void create(PrestamoAuto prestamoAuto);

    void edit(PrestamoAuto prestamoAuto);

    void remove(PrestamoAuto prestamoAuto);

    PrestamoAuto find(Object id);

    List<PrestamoAuto> findAll();

    List<PrestamoAuto> findRange(int[] range);

    int count();
    
}
