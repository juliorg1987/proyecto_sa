/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author victor
 */
@Local
public interface TipoSeguroFacadeLocal {

    void create(TipoSeguro tipoSeguro);

    void edit(TipoSeguro tipoSeguro);

    void remove(TipoSeguro tipoSeguro);

    TipoSeguro find(Object id);

    List<TipoSeguro> findAll();

    List<TipoSeguro> findRange(int[] range);

    int count();
    
}
