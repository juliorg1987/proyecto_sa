/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author victor
 */
@Stateless
public class PrestamoHipotecaCatalogoFacade extends AbstractFacade<PrestamoHipotecaCatalogo> implements PrestamoHipotecaCatalogoFacadeLocal {
    @PersistenceContext(unitName = "ProyectoSA-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrestamoHipotecaCatalogoFacade() {
        super(PrestamoHipotecaCatalogo.class);
    }

    @Override
    public List<PrestamoHipotecaCatalogo> findBetweenDataRange(long preciobase, long area) {
        Query query = getEntityManager().createQuery("SELECT p FROM PrestamoHipotecaCatalogo p WHERE :modelo1 >= p.aniominimo AND :modelo2 <= p.aniomaximo AND :preciobase1 >= p.preciobasemin AND :preciobase2 <= p.preciobasemax");
        query.setParameter("modelo1", area);
        query.setParameter("modelo2", area);
        query.setParameter("preciobase1", preciobase);
        query.setParameter("preciobase2", preciobase);
        return query.getResultList();
    }
    
}
