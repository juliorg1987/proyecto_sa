/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author victor
 */
@Entity
@Table(name = "PrestamoAuto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PrestamoAuto.findAll", query = "SELECT p FROM PrestamoAuto p")})
public class PrestamoAuto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPrestamoAuto")
    private Integer idPrestamoAuto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modelo")
    private short modelo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "placa")
    private String placa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 24)
    @Column(name = "chasis")
    private String chasis;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto")
    private BigDecimal monto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numPagos")
    private int numPagos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "interes")
    private short interes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private short estado;
    @JoinColumn(name = "convenio", referencedColumnName = "convenio")
    @ManyToOne(optional = false)
    private Usuario convenio;
    @OneToMany(mappedBy = "idPrestamoAuto")
    private List<Transaccion> transaccionList;

    public PrestamoAuto() {
    }

    public PrestamoAuto(Integer idPrestamoAuto) {
        this.idPrestamoAuto = idPrestamoAuto;
    }

    public PrestamoAuto(Integer idPrestamoAuto, short modelo, String placa, String chasis, BigDecimal monto, int numPagos, short interes, short estado) {
        this.idPrestamoAuto = idPrestamoAuto;
        this.modelo = modelo;
        this.placa = placa;
        this.chasis = chasis;
        this.monto = monto;
        this.numPagos = numPagos;
        this.interes = interes;
        this.estado = estado;
    }

    public Integer getIdPrestamoAuto() {
        return idPrestamoAuto;
    }

    public void setIdPrestamoAuto(Integer idPrestamoAuto) {
        this.idPrestamoAuto = idPrestamoAuto;
    }

    public short getModelo() {
        return modelo;
    }

    public void setModelo(short modelo) {
        this.modelo = modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public int getNumPagos() {
        return numPagos;
    }

    public void setNumPagos(int numPagos) {
        this.numPagos = numPagos;
    }

    public short getInteres() {
        return interes;
    }

    public void setInteres(short interes) {
        this.interes = interes;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    public Usuario getConvenio() {
        return convenio;
    }

    public void setConvenio(Usuario convenio) {
        this.convenio = convenio;
    }

    @XmlTransient
    public List<Transaccion> getTransaccionList() {
        return transaccionList;
    }

    public void setTransaccionList(List<Transaccion> transaccionList) {
        this.transaccionList = transaccionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPrestamoAuto != null ? idPrestamoAuto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrestamoAuto)) {
            return false;
        }
        PrestamoAuto other = (PrestamoAuto) object;
        if ((this.idPrestamoAuto == null && other.idPrestamoAuto != null) || (this.idPrestamoAuto != null && !this.idPrestamoAuto.equals(other.idPrestamoAuto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ejb.PrestamoAuto[ idPrestamoAuto=" + idPrestamoAuto + " ]";
    }
    
}
