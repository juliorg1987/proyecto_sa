/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package aplicacion;

/**
 *
 * @author victor
 */
public class ManejadorWSNosotros {

    public static String agregarSeguroVida(java.lang.String token, int convenio, int codigoSeguro) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.agregarSeguroVida(token, convenio, codigoSeguro);
    }

    public static String aperturaDeCuenta(java.lang.String token, long dpi, java.lang.String nombre, java.lang.String direccion, java.lang.String telefono) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.aperturaDeCuenta(token, dpi, nombre, direccion, telefono);
    }

    public static String autenticar(int convenio, java.lang.String password) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.autenticar(convenio, password);
    }

    public static String consultaSeguroVida(java.lang.String token, int codigo) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.consultaSeguroVida(token, codigo);
    }

    public static String consultarCuenta(java.lang.String token, int cuenta) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.consultarCuenta(token, cuenta);
    }

    public static String consultarHipotecaDelCatalogo(java.lang.String token, long area, long precioBase) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.consultarHipotecaDelCatalogo(token, area, precioBase);
    }

    public static String consultarPrestamoAutomovil(java.lang.String token, long precioBase, short modelo) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.consultarPrestamoAutomovil(token, precioBase, modelo);
    }

    public static String consultarSeguroDeVidaEspecifico(java.lang.String token, int usuarioseguro) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.consultarSeguroDeVidaEspecifico(token, usuarioseguro);
    }

    public static String crearConvenio(long dpi, java.lang.String nombre1, java.lang.String nombre2, java.lang.String apellido1, java.lang.String apellido2, java.lang.String password) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.crearConvenio(dpi, nombre1, nombre2, apellido1, apellido2, password);
    }

    public static String cuentasPorConvenio(java.lang.String token, int convenio) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.cuentasPorConvenio(token, convenio);
    }

    public static String depositos(java.lang.String token, int numCuenta, float monto, java.lang.String descripcion) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.depositos(token, numCuenta, monto, descripcion);
    }

    public static String pagarSeguroVida(java.lang.String token, int usuarioseguro, int monto, java.lang.String descripcion) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.pagarSeguroVida(token, usuarioseguro, monto, descripcion);
    }

    public static String prestamoAutomovilPorConvenio(java.lang.String token, int convenio) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.prestamoAutomovilPorConvenio(token, convenio);
    }

    public static String realizarPrestamoAutomovil(java.lang.String token, int convenio, short modelo, java.lang.String placa, java.lang.String chasis, long monto, int numPagos, short interes) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.realizarPrestamoAutomovil(token, convenio, modelo, placa, chasis, monto, numPagos, interes);
    }

    public static String realizarPrestamoHipotecario(java.lang.String token, int convenio, java.lang.String descripcion, int area, java.lang.String direccion, long monto, int numPagos, short interes) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.realizarPrestamoHipotecario(token, convenio, descripcion, area, direccion, monto, numPagos, interes);
    }

    public static String segurosVidaPorConvenio(java.lang.String token, int convenio) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.segurosVidaPorConvenio(token, convenio);
    }

    
}
