
package ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for realizarPrestamoHipotecario complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="realizarPrestamoHipotecario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="convenio" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="direccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="monto" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="numPagos" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="interes" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "realizarPrestamoHipotecario", propOrder = {
    "token",
    "convenio",
    "descripcion",
    "area",
    "direccion",
    "monto",
    "numPagos",
    "interes"
})
public class RealizarPrestamoHipotecario {

    protected String token;
    protected int convenio;
    protected String descripcion;
    protected int area;
    protected String direccion;
    protected long monto;
    protected int numPagos;
    protected short interes;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the convenio property.
     * 
     */
    public int getConvenio() {
        return convenio;
    }

    /**
     * Sets the value of the convenio property.
     * 
     */
    public void setConvenio(int value) {
        this.convenio = value;
    }

    /**
     * Gets the value of the descripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Gets the value of the area property.
     * 
     */
    public int getArea() {
        return area;
    }

    /**
     * Sets the value of the area property.
     * 
     */
    public void setArea(int value) {
        this.area = value;
    }

    /**
     * Gets the value of the direccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Sets the value of the direccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion(String value) {
        this.direccion = value;
    }

    /**
     * Gets the value of the monto property.
     * 
     */
    public long getMonto() {
        return monto;
    }

    /**
     * Sets the value of the monto property.
     * 
     */
    public void setMonto(long value) {
        this.monto = value;
    }

    /**
     * Gets the value of the numPagos property.
     * 
     */
    public int getNumPagos() {
        return numPagos;
    }

    /**
     * Sets the value of the numPagos property.
     * 
     */
    public void setNumPagos(int value) {
        this.numPagos = value;
    }

    /**
     * Gets the value of the interes property.
     * 
     */
    public short getInteres() {
        return interes;
    }

    /**
     * Sets the value of the interes property.
     * 
     */
    public void setInteres(short value) {
        this.interes = value;
    }

}
