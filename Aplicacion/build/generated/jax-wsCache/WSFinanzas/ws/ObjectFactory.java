
package ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PrestamoAutomovilPorConvenioResponse_QNAME = new QName("http://ws/", "prestamoAutomovilPorConvenioResponse");
    private final static QName _PrestamoHipotecarioPorConvenio_QNAME = new QName("http://ws/", "prestamoHipotecarioPorConvenio");
    private final static QName _PrestamoAutomovilPorConvenio_QNAME = new QName("http://ws/", "prestamoAutomovilPorConvenio");
    private final static QName _PagarSeguroVidaResponse_QNAME = new QName("http://ws/", "pagarSeguroVidaResponse");
    private final static QName _CuentasPorConvenio_QNAME = new QName("http://ws/", "cuentasPorConvenio");
    private final static QName _DepositosResponse_QNAME = new QName("http://ws/", "depositosResponse");
    private final static QName _AperturaDeCuenta_QNAME = new QName("http://ws/", "aperturaDeCuenta");
    private final static QName _RealizarPrestamoHipotecarioResponse_QNAME = new QName("http://ws/", "realizarPrestamoHipotecarioResponse");
    private final static QName _AutenticarResponse_QNAME = new QName("http://ws/", "autenticarResponse");
    private final static QName _ConsultaSeguroVidaResponse_QNAME = new QName("http://ws/", "consultaSeguroVidaResponse");
    private final static QName _RealizarPrestamoHipotecario_QNAME = new QName("http://ws/", "realizarPrestamoHipotecario");
    private final static QName _PrestamoHipotecarioPorConvenioResponse_QNAME = new QName("http://ws/", "prestamoHipotecarioPorConvenioResponse");
    private final static QName _CrearConvenio_QNAME = new QName("http://ws/", "crearConvenio");
    private final static QName _AgregarSeguroVida_QNAME = new QName("http://ws/", "agregarSeguroVida");
    private final static QName _SegurosVidaPorConvenioResponse_QNAME = new QName("http://ws/", "segurosVidaPorConvenioResponse");
    private final static QName _ConsultarPrestamoAutomovil_QNAME = new QName("http://ws/", "consultarPrestamoAutomovil");
    private final static QName _RealizarPrestamoAutomovil_QNAME = new QName("http://ws/", "realizarPrestamoAutomovil");
    private final static QName _AgregarSeguroVidaResponse_QNAME = new QName("http://ws/", "agregarSeguroVidaResponse");
    private final static QName _Depositos_QNAME = new QName("http://ws/", "depositos");
    private final static QName _CrearConvenioResponse_QNAME = new QName("http://ws/", "crearConvenioResponse");
    private final static QName _Autenticar_QNAME = new QName("http://ws/", "autenticar");
    private final static QName _ConsultarHipotecaDelCatalogoResponse_QNAME = new QName("http://ws/", "consultarHipotecaDelCatalogoResponse");
    private final static QName _ConsultarPrestamoAutomovilResponse_QNAME = new QName("http://ws/", "consultarPrestamoAutomovilResponse");
    private final static QName _ConsultarSeguroDeVidaEspecificoResponse_QNAME = new QName("http://ws/", "consultarSeguroDeVidaEspecificoResponse");
    private final static QName _PagarSeguroVida_QNAME = new QName("http://ws/", "pagarSeguroVida");
    private final static QName _ConsultarSeguroDeVidaEspecifico_QNAME = new QName("http://ws/", "consultarSeguroDeVidaEspecifico");
    private final static QName _ConsultarCuenta_QNAME = new QName("http://ws/", "consultarCuenta");
    private final static QName _RealizarPrestamoAutomovilResponse_QNAME = new QName("http://ws/", "realizarPrestamoAutomovilResponse");
    private final static QName _CuentasPorConvenioResponse_QNAME = new QName("http://ws/", "cuentasPorConvenioResponse");
    private final static QName _ConsultarHipotecaDelCatalogo_QNAME = new QName("http://ws/", "consultarHipotecaDelCatalogo");
    private final static QName _ConsultarCuentaResponse_QNAME = new QName("http://ws/", "consultarCuentaResponse");
    private final static QName _SegurosVidaPorConvenio_QNAME = new QName("http://ws/", "segurosVidaPorConvenio");
    private final static QName _ConsultaSeguroVida_QNAME = new QName("http://ws/", "consultaSeguroVida");
    private final static QName _AperturaDeCuentaResponse_QNAME = new QName("http://ws/", "aperturaDeCuentaResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AgregarSeguroVidaResponse }
     * 
     */
    public AgregarSeguroVidaResponse createAgregarSeguroVidaResponse() {
        return new AgregarSeguroVidaResponse();
    }

    /**
     * Create an instance of {@link RealizarPrestamoAutomovil }
     * 
     */
    public RealizarPrestamoAutomovil createRealizarPrestamoAutomovil() {
        return new RealizarPrestamoAutomovil();
    }

    /**
     * Create an instance of {@link ConsultarPrestamoAutomovil }
     * 
     */
    public ConsultarPrestamoAutomovil createConsultarPrestamoAutomovil() {
        return new ConsultarPrestamoAutomovil();
    }

    /**
     * Create an instance of {@link SegurosVidaPorConvenioResponse }
     * 
     */
    public SegurosVidaPorConvenioResponse createSegurosVidaPorConvenioResponse() {
        return new SegurosVidaPorConvenioResponse();
    }

    /**
     * Create an instance of {@link PrestamoHipotecarioPorConvenioResponse }
     * 
     */
    public PrestamoHipotecarioPorConvenioResponse createPrestamoHipotecarioPorConvenioResponse() {
        return new PrestamoHipotecarioPorConvenioResponse();
    }

    /**
     * Create an instance of {@link AgregarSeguroVida }
     * 
     */
    public AgregarSeguroVida createAgregarSeguroVida() {
        return new AgregarSeguroVida();
    }

    /**
     * Create an instance of {@link CrearConvenio }
     * 
     */
    public CrearConvenio createCrearConvenio() {
        return new CrearConvenio();
    }

    /**
     * Create an instance of {@link RealizarPrestamoHipotecario }
     * 
     */
    public RealizarPrestamoHipotecario createRealizarPrestamoHipotecario() {
        return new RealizarPrestamoHipotecario();
    }

    /**
     * Create an instance of {@link ConsultaSeguroVidaResponse }
     * 
     */
    public ConsultaSeguroVidaResponse createConsultaSeguroVidaResponse() {
        return new ConsultaSeguroVidaResponse();
    }

    /**
     * Create an instance of {@link RealizarPrestamoHipotecarioResponse }
     * 
     */
    public RealizarPrestamoHipotecarioResponse createRealizarPrestamoHipotecarioResponse() {
        return new RealizarPrestamoHipotecarioResponse();
    }

    /**
     * Create an instance of {@link AutenticarResponse }
     * 
     */
    public AutenticarResponse createAutenticarResponse() {
        return new AutenticarResponse();
    }

    /**
     * Create an instance of {@link DepositosResponse }
     * 
     */
    public DepositosResponse createDepositosResponse() {
        return new DepositosResponse();
    }

    /**
     * Create an instance of {@link AperturaDeCuenta }
     * 
     */
    public AperturaDeCuenta createAperturaDeCuenta() {
        return new AperturaDeCuenta();
    }

    /**
     * Create an instance of {@link PrestamoAutomovilPorConvenio }
     * 
     */
    public PrestamoAutomovilPorConvenio createPrestamoAutomovilPorConvenio() {
        return new PrestamoAutomovilPorConvenio();
    }

    /**
     * Create an instance of {@link PagarSeguroVidaResponse }
     * 
     */
    public PagarSeguroVidaResponse createPagarSeguroVidaResponse() {
        return new PagarSeguroVidaResponse();
    }

    /**
     * Create an instance of {@link CuentasPorConvenio }
     * 
     */
    public CuentasPorConvenio createCuentasPorConvenio() {
        return new CuentasPorConvenio();
    }

    /**
     * Create an instance of {@link PrestamoAutomovilPorConvenioResponse }
     * 
     */
    public PrestamoAutomovilPorConvenioResponse createPrestamoAutomovilPorConvenioResponse() {
        return new PrestamoAutomovilPorConvenioResponse();
    }

    /**
     * Create an instance of {@link PrestamoHipotecarioPorConvenio }
     * 
     */
    public PrestamoHipotecarioPorConvenio createPrestamoHipotecarioPorConvenio() {
        return new PrestamoHipotecarioPorConvenio();
    }

    /**
     * Create an instance of {@link ConsultaSeguroVida }
     * 
     */
    public ConsultaSeguroVida createConsultaSeguroVida() {
        return new ConsultaSeguroVida();
    }

    /**
     * Create an instance of {@link AperturaDeCuentaResponse }
     * 
     */
    public AperturaDeCuentaResponse createAperturaDeCuentaResponse() {
        return new AperturaDeCuentaResponse();
    }

    /**
     * Create an instance of {@link SegurosVidaPorConvenio }
     * 
     */
    public SegurosVidaPorConvenio createSegurosVidaPorConvenio() {
        return new SegurosVidaPorConvenio();
    }

    /**
     * Create an instance of {@link ConsultarCuentaResponse }
     * 
     */
    public ConsultarCuentaResponse createConsultarCuentaResponse() {
        return new ConsultarCuentaResponse();
    }

    /**
     * Create an instance of {@link ConsultarHipotecaDelCatalogo }
     * 
     */
    public ConsultarHipotecaDelCatalogo createConsultarHipotecaDelCatalogo() {
        return new ConsultarHipotecaDelCatalogo();
    }

    /**
     * Create an instance of {@link CuentasPorConvenioResponse }
     * 
     */
    public CuentasPorConvenioResponse createCuentasPorConvenioResponse() {
        return new CuentasPorConvenioResponse();
    }

    /**
     * Create an instance of {@link RealizarPrestamoAutomovilResponse }
     * 
     */
    public RealizarPrestamoAutomovilResponse createRealizarPrestamoAutomovilResponse() {
        return new RealizarPrestamoAutomovilResponse();
    }

    /**
     * Create an instance of {@link ConsultarCuenta }
     * 
     */
    public ConsultarCuenta createConsultarCuenta() {
        return new ConsultarCuenta();
    }

    /**
     * Create an instance of {@link PagarSeguroVida }
     * 
     */
    public PagarSeguroVida createPagarSeguroVida() {
        return new PagarSeguroVida();
    }

    /**
     * Create an instance of {@link ConsultarSeguroDeVidaEspecifico }
     * 
     */
    public ConsultarSeguroDeVidaEspecifico createConsultarSeguroDeVidaEspecifico() {
        return new ConsultarSeguroDeVidaEspecifico();
    }

    /**
     * Create an instance of {@link ConsultarSeguroDeVidaEspecificoResponse }
     * 
     */
    public ConsultarSeguroDeVidaEspecificoResponse createConsultarSeguroDeVidaEspecificoResponse() {
        return new ConsultarSeguroDeVidaEspecificoResponse();
    }

    /**
     * Create an instance of {@link ConsultarHipotecaDelCatalogoResponse }
     * 
     */
    public ConsultarHipotecaDelCatalogoResponse createConsultarHipotecaDelCatalogoResponse() {
        return new ConsultarHipotecaDelCatalogoResponse();
    }

    /**
     * Create an instance of {@link Autenticar }
     * 
     */
    public Autenticar createAutenticar() {
        return new Autenticar();
    }

    /**
     * Create an instance of {@link ConsultarPrestamoAutomovilResponse }
     * 
     */
    public ConsultarPrestamoAutomovilResponse createConsultarPrestamoAutomovilResponse() {
        return new ConsultarPrestamoAutomovilResponse();
    }

    /**
     * Create an instance of {@link CrearConvenioResponse }
     * 
     */
    public CrearConvenioResponse createCrearConvenioResponse() {
        return new CrearConvenioResponse();
    }

    /**
     * Create an instance of {@link Depositos }
     * 
     */
    public Depositos createDepositos() {
        return new Depositos();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrestamoAutomovilPorConvenioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "prestamoAutomovilPorConvenioResponse")
    public JAXBElement<PrestamoAutomovilPorConvenioResponse> createPrestamoAutomovilPorConvenioResponse(PrestamoAutomovilPorConvenioResponse value) {
        return new JAXBElement<PrestamoAutomovilPorConvenioResponse>(_PrestamoAutomovilPorConvenioResponse_QNAME, PrestamoAutomovilPorConvenioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrestamoHipotecarioPorConvenio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "prestamoHipotecarioPorConvenio")
    public JAXBElement<PrestamoHipotecarioPorConvenio> createPrestamoHipotecarioPorConvenio(PrestamoHipotecarioPorConvenio value) {
        return new JAXBElement<PrestamoHipotecarioPorConvenio>(_PrestamoHipotecarioPorConvenio_QNAME, PrestamoHipotecarioPorConvenio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrestamoAutomovilPorConvenio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "prestamoAutomovilPorConvenio")
    public JAXBElement<PrestamoAutomovilPorConvenio> createPrestamoAutomovilPorConvenio(PrestamoAutomovilPorConvenio value) {
        return new JAXBElement<PrestamoAutomovilPorConvenio>(_PrestamoAutomovilPorConvenio_QNAME, PrestamoAutomovilPorConvenio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagarSeguroVidaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "pagarSeguroVidaResponse")
    public JAXBElement<PagarSeguroVidaResponse> createPagarSeguroVidaResponse(PagarSeguroVidaResponse value) {
        return new JAXBElement<PagarSeguroVidaResponse>(_PagarSeguroVidaResponse_QNAME, PagarSeguroVidaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CuentasPorConvenio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "cuentasPorConvenio")
    public JAXBElement<CuentasPorConvenio> createCuentasPorConvenio(CuentasPorConvenio value) {
        return new JAXBElement<CuentasPorConvenio>(_CuentasPorConvenio_QNAME, CuentasPorConvenio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DepositosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "depositosResponse")
    public JAXBElement<DepositosResponse> createDepositosResponse(DepositosResponse value) {
        return new JAXBElement<DepositosResponse>(_DepositosResponse_QNAME, DepositosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AperturaDeCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "aperturaDeCuenta")
    public JAXBElement<AperturaDeCuenta> createAperturaDeCuenta(AperturaDeCuenta value) {
        return new JAXBElement<AperturaDeCuenta>(_AperturaDeCuenta_QNAME, AperturaDeCuenta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RealizarPrestamoHipotecarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "realizarPrestamoHipotecarioResponse")
    public JAXBElement<RealizarPrestamoHipotecarioResponse> createRealizarPrestamoHipotecarioResponse(RealizarPrestamoHipotecarioResponse value) {
        return new JAXBElement<RealizarPrestamoHipotecarioResponse>(_RealizarPrestamoHipotecarioResponse_QNAME, RealizarPrestamoHipotecarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AutenticarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "autenticarResponse")
    public JAXBElement<AutenticarResponse> createAutenticarResponse(AutenticarResponse value) {
        return new JAXBElement<AutenticarResponse>(_AutenticarResponse_QNAME, AutenticarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaSeguroVidaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultaSeguroVidaResponse")
    public JAXBElement<ConsultaSeguroVidaResponse> createConsultaSeguroVidaResponse(ConsultaSeguroVidaResponse value) {
        return new JAXBElement<ConsultaSeguroVidaResponse>(_ConsultaSeguroVidaResponse_QNAME, ConsultaSeguroVidaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RealizarPrestamoHipotecario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "realizarPrestamoHipotecario")
    public JAXBElement<RealizarPrestamoHipotecario> createRealizarPrestamoHipotecario(RealizarPrestamoHipotecario value) {
        return new JAXBElement<RealizarPrestamoHipotecario>(_RealizarPrestamoHipotecario_QNAME, RealizarPrestamoHipotecario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrestamoHipotecarioPorConvenioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "prestamoHipotecarioPorConvenioResponse")
    public JAXBElement<PrestamoHipotecarioPorConvenioResponse> createPrestamoHipotecarioPorConvenioResponse(PrestamoHipotecarioPorConvenioResponse value) {
        return new JAXBElement<PrestamoHipotecarioPorConvenioResponse>(_PrestamoHipotecarioPorConvenioResponse_QNAME, PrestamoHipotecarioPorConvenioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearConvenio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "crearConvenio")
    public JAXBElement<CrearConvenio> createCrearConvenio(CrearConvenio value) {
        return new JAXBElement<CrearConvenio>(_CrearConvenio_QNAME, CrearConvenio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarSeguroVida }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "agregarSeguroVida")
    public JAXBElement<AgregarSeguroVida> createAgregarSeguroVida(AgregarSeguroVida value) {
        return new JAXBElement<AgregarSeguroVida>(_AgregarSeguroVida_QNAME, AgregarSeguroVida.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SegurosVidaPorConvenioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "segurosVidaPorConvenioResponse")
    public JAXBElement<SegurosVidaPorConvenioResponse> createSegurosVidaPorConvenioResponse(SegurosVidaPorConvenioResponse value) {
        return new JAXBElement<SegurosVidaPorConvenioResponse>(_SegurosVidaPorConvenioResponse_QNAME, SegurosVidaPorConvenioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPrestamoAutomovil }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultarPrestamoAutomovil")
    public JAXBElement<ConsultarPrestamoAutomovil> createConsultarPrestamoAutomovil(ConsultarPrestamoAutomovil value) {
        return new JAXBElement<ConsultarPrestamoAutomovil>(_ConsultarPrestamoAutomovil_QNAME, ConsultarPrestamoAutomovil.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RealizarPrestamoAutomovil }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "realizarPrestamoAutomovil")
    public JAXBElement<RealizarPrestamoAutomovil> createRealizarPrestamoAutomovil(RealizarPrestamoAutomovil value) {
        return new JAXBElement<RealizarPrestamoAutomovil>(_RealizarPrestamoAutomovil_QNAME, RealizarPrestamoAutomovil.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarSeguroVidaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "agregarSeguroVidaResponse")
    public JAXBElement<AgregarSeguroVidaResponse> createAgregarSeguroVidaResponse(AgregarSeguroVidaResponse value) {
        return new JAXBElement<AgregarSeguroVidaResponse>(_AgregarSeguroVidaResponse_QNAME, AgregarSeguroVidaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Depositos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "depositos")
    public JAXBElement<Depositos> createDepositos(Depositos value) {
        return new JAXBElement<Depositos>(_Depositos_QNAME, Depositos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearConvenioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "crearConvenioResponse")
    public JAXBElement<CrearConvenioResponse> createCrearConvenioResponse(CrearConvenioResponse value) {
        return new JAXBElement<CrearConvenioResponse>(_CrearConvenioResponse_QNAME, CrearConvenioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Autenticar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "autenticar")
    public JAXBElement<Autenticar> createAutenticar(Autenticar value) {
        return new JAXBElement<Autenticar>(_Autenticar_QNAME, Autenticar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarHipotecaDelCatalogoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultarHipotecaDelCatalogoResponse")
    public JAXBElement<ConsultarHipotecaDelCatalogoResponse> createConsultarHipotecaDelCatalogoResponse(ConsultarHipotecaDelCatalogoResponse value) {
        return new JAXBElement<ConsultarHipotecaDelCatalogoResponse>(_ConsultarHipotecaDelCatalogoResponse_QNAME, ConsultarHipotecaDelCatalogoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPrestamoAutomovilResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultarPrestamoAutomovilResponse")
    public JAXBElement<ConsultarPrestamoAutomovilResponse> createConsultarPrestamoAutomovilResponse(ConsultarPrestamoAutomovilResponse value) {
        return new JAXBElement<ConsultarPrestamoAutomovilResponse>(_ConsultarPrestamoAutomovilResponse_QNAME, ConsultarPrestamoAutomovilResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarSeguroDeVidaEspecificoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultarSeguroDeVidaEspecificoResponse")
    public JAXBElement<ConsultarSeguroDeVidaEspecificoResponse> createConsultarSeguroDeVidaEspecificoResponse(ConsultarSeguroDeVidaEspecificoResponse value) {
        return new JAXBElement<ConsultarSeguroDeVidaEspecificoResponse>(_ConsultarSeguroDeVidaEspecificoResponse_QNAME, ConsultarSeguroDeVidaEspecificoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagarSeguroVida }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "pagarSeguroVida")
    public JAXBElement<PagarSeguroVida> createPagarSeguroVida(PagarSeguroVida value) {
        return new JAXBElement<PagarSeguroVida>(_PagarSeguroVida_QNAME, PagarSeguroVida.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarSeguroDeVidaEspecifico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultarSeguroDeVidaEspecifico")
    public JAXBElement<ConsultarSeguroDeVidaEspecifico> createConsultarSeguroDeVidaEspecifico(ConsultarSeguroDeVidaEspecifico value) {
        return new JAXBElement<ConsultarSeguroDeVidaEspecifico>(_ConsultarSeguroDeVidaEspecifico_QNAME, ConsultarSeguroDeVidaEspecifico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultarCuenta")
    public JAXBElement<ConsultarCuenta> createConsultarCuenta(ConsultarCuenta value) {
        return new JAXBElement<ConsultarCuenta>(_ConsultarCuenta_QNAME, ConsultarCuenta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RealizarPrestamoAutomovilResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "realizarPrestamoAutomovilResponse")
    public JAXBElement<RealizarPrestamoAutomovilResponse> createRealizarPrestamoAutomovilResponse(RealizarPrestamoAutomovilResponse value) {
        return new JAXBElement<RealizarPrestamoAutomovilResponse>(_RealizarPrestamoAutomovilResponse_QNAME, RealizarPrestamoAutomovilResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CuentasPorConvenioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "cuentasPorConvenioResponse")
    public JAXBElement<CuentasPorConvenioResponse> createCuentasPorConvenioResponse(CuentasPorConvenioResponse value) {
        return new JAXBElement<CuentasPorConvenioResponse>(_CuentasPorConvenioResponse_QNAME, CuentasPorConvenioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarHipotecaDelCatalogo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultarHipotecaDelCatalogo")
    public JAXBElement<ConsultarHipotecaDelCatalogo> createConsultarHipotecaDelCatalogo(ConsultarHipotecaDelCatalogo value) {
        return new JAXBElement<ConsultarHipotecaDelCatalogo>(_ConsultarHipotecaDelCatalogo_QNAME, ConsultarHipotecaDelCatalogo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarCuentaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultarCuentaResponse")
    public JAXBElement<ConsultarCuentaResponse> createConsultarCuentaResponse(ConsultarCuentaResponse value) {
        return new JAXBElement<ConsultarCuentaResponse>(_ConsultarCuentaResponse_QNAME, ConsultarCuentaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SegurosVidaPorConvenio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "segurosVidaPorConvenio")
    public JAXBElement<SegurosVidaPorConvenio> createSegurosVidaPorConvenio(SegurosVidaPorConvenio value) {
        return new JAXBElement<SegurosVidaPorConvenio>(_SegurosVidaPorConvenio_QNAME, SegurosVidaPorConvenio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaSeguroVida }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultaSeguroVida")
    public JAXBElement<ConsultaSeguroVida> createConsultaSeguroVida(ConsultaSeguroVida value) {
        return new JAXBElement<ConsultaSeguroVida>(_ConsultaSeguroVida_QNAME, ConsultaSeguroVida.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AperturaDeCuentaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "aperturaDeCuentaResponse")
    public JAXBElement<AperturaDeCuentaResponse> createAperturaDeCuentaResponse(AperturaDeCuentaResponse value) {
        return new JAXBElement<AperturaDeCuentaResponse>(_AperturaDeCuentaResponse_QNAME, AperturaDeCuentaResponse.class, null, value);
    }

}
