
package ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for realizarPrestamoAutomovil complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="realizarPrestamoAutomovil">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="convenio" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="modelo" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="placa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chasis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="monto" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="numPagos" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="interes" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "realizarPrestamoAutomovil", propOrder = {
    "token",
    "convenio",
    "modelo",
    "placa",
    "chasis",
    "monto",
    "numPagos",
    "interes"
})
public class RealizarPrestamoAutomovil {

    protected String token;
    protected int convenio;
    protected short modelo;
    protected String placa;
    protected String chasis;
    protected long monto;
    protected int numPagos;
    protected short interes;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the convenio property.
     * 
     */
    public int getConvenio() {
        return convenio;
    }

    /**
     * Sets the value of the convenio property.
     * 
     */
    public void setConvenio(int value) {
        this.convenio = value;
    }

    /**
     * Gets the value of the modelo property.
     * 
     */
    public short getModelo() {
        return modelo;
    }

    /**
     * Sets the value of the modelo property.
     * 
     */
    public void setModelo(short value) {
        this.modelo = value;
    }

    /**
     * Gets the value of the placa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * Sets the value of the placa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaca(String value) {
        this.placa = value;
    }

    /**
     * Gets the value of the chasis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChasis() {
        return chasis;
    }

    /**
     * Sets the value of the chasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChasis(String value) {
        this.chasis = value;
    }

    /**
     * Gets the value of the monto property.
     * 
     */
    public long getMonto() {
        return monto;
    }

    /**
     * Sets the value of the monto property.
     * 
     */
    public void setMonto(long value) {
        this.monto = value;
    }

    /**
     * Gets the value of the numPagos property.
     * 
     */
    public int getNumPagos() {
        return numPagos;
    }

    /**
     * Sets the value of the numPagos property.
     * 
     */
    public void setNumPagos(int value) {
        this.numPagos = value;
    }

    /**
     * Gets the value of the interes property.
     * 
     */
    public short getInteres() {
        return interes;
    }

    /**
     * Sets the value of the interes property.
     * 
     */
    public void setInteres(short value) {
        this.interes = value;
    }

}
