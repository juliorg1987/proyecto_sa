
package ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consultarPrestamoAutomovil complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consultarPrestamoAutomovil">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="precioBase" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="modelo" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultarPrestamoAutomovil", propOrder = {
    "token",
    "precioBase",
    "modelo"
})
public class ConsultarPrestamoAutomovil {

    protected String token;
    protected long precioBase;
    protected short modelo;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the precioBase property.
     * 
     */
    public long getPrecioBase() {
        return precioBase;
    }

    /**
     * Sets the value of the precioBase property.
     * 
     */
    public void setPrecioBase(long value) {
        this.precioBase = value;
    }

    /**
     * Gets the value of the modelo property.
     * 
     */
    public short getModelo() {
        return modelo;
    }

    /**
     * Sets the value of the modelo property.
     * 
     */
    public void setModelo(short value) {
        this.modelo = value;
    }

}
