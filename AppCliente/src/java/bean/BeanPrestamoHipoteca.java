/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@ViewScoped
public class BeanPrestamoHipoteca {
    
    

   private String token;
    private int convenio;
    private String descripcion;
    private int area;
    private String direccion;
    private long monto;
    private int numPagos;
    private short intereses;
    
    public BeanPrestamoHipoteca() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getConvenio() {
        return convenio;
    }

    public void setConvenio(int convenio) {
        this.convenio = convenio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public long getMonto() {
        return monto;
    }

    public void setMonto(long monto) {
        this.monto = monto;
    }

    public int getNumPagos() {
        return numPagos;
    }

    public void setNumPagos(int numPagos) {
        this.numPagos = numPagos;
    }

    public short getIntereses() {
        return intereses;
    }

    public void setIntereses(short intereses) {
        this.intereses = intereses;
    }

   
    
    public String realizacionPrestamoHipoteca(){
        llamadasWSDL.realizarPrestamoHipotecario(token, convenio, descripcion, area, direccion, monto, numPagos, intereses);
        return "menu";
        
    }
    
}
