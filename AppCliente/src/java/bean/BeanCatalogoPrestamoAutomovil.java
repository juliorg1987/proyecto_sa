/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@ViewScoped
public class BeanCatalogoPrestamoAutomovil {

    /**
     * Creates a new instance of BeanCatalogoPrestamoAutomovil
     */
    
    private String token;
    private long precioBase;
    private short modelo;
    
    public BeanCatalogoPrestamoAutomovil() {
        
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(long precioBase) {
        this.precioBase = precioBase;
    }

    public short getModelo() {
        return modelo;
    }

    public void setModelo(short modelo) {
        this.modelo = modelo;
    }

    public String obtenerCatalogoPrestamoAutomovil(){
        llamadasWSDL.consultarPrestamoAutomovil(token, precioBase, modelo);
        return "desplegarCatalogo";
        
    }
    
}
