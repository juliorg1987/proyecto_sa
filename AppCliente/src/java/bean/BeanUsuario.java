/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@SessionScoped
public class BeanUsuario implements Serializable{
    
    private int convenio;
    private String passwd;
    
    private boolean autenticado;
    private String token;

    private String mensaje;
    
    /**
     * Creates a new instance of BeanUsuario
     */
    public BeanUsuario() {
    }

    public int getConvenio() {
        return convenio;
    }

    public void setConvenio(int convenio) {
        this.convenio = convenio;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public boolean isAutenticado() {
        return autenticado;
    }

    public void setAutenticado(boolean autenticado) {
        this.autenticado = autenticado;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMensaje() {
        String mensajeTmp = mensaje;
        mensaje = null;
        return mensajeTmp;
    }

    public String procesar(){
        String respuesta = llamadasWSDL.autenticar(convenio, passwd);
        mensaje = respuesta;
        return null;
    }
    
    
    
}
