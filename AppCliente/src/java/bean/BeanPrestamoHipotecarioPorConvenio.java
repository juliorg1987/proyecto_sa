/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@RequestScoped
public class BeanPrestamoHipotecarioPorConvenio {

    private String token;
    private int convenio;
    public BeanPrestamoHipotecarioPorConvenio() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getConvenio() {
        return convenio;
    }

    public void setConvenio(int convenio) {
        this.convenio = convenio;
    }

    
    
    public String realizarPrestamoHipotecarioPorConvenio(){
        
        llamadasWSDL.prestamoHipotecarioPorConvenio(token, convenio);
        return "menu";
    }
}
