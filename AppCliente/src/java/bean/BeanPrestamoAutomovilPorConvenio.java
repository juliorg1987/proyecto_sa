/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@ViewScoped
public class BeanPrestamoAutomovilPorConvenio {

    private String token;
    private int convenio;
    
    public BeanPrestamoAutomovilPorConvenio() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getConvenio() {
        return convenio;
    }

    public void setConvenio(int convenio) {
        this.convenio = convenio;
    }
    
    public String realizarPrestamoAutomovilPorConvenio(){
        llamadasWSDL.prestamoAutomovilPorConvenio(token, convenio);
        return "menu";
    }
}
