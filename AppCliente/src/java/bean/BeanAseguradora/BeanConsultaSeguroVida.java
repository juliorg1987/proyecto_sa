/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean.BeanAseguradora;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@ViewScoped
public class BeanConsultaSeguroVida {

    /**
     * Creates a new instance of BeanConsultaSeguroVida
     */
    
    private String token;
    private int codigoSeguro;
    
    public BeanConsultaSeguroVida() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getCodigoSeguro() {
        return codigoSeguro;
    }

    public void setCodigoSeguro(int codigoSeguro) {
        this.codigoSeguro = codigoSeguro;
    }
    
    
    
}
