/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean.BeanAseguradora;

/**
 *
 * @author Roberto
 */
public class llamadasAseguradoraWSDL {

    public static String consultaSeguroVida(java.lang.String token, int codigo) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.consultaSeguroVida(token, codigo);
    }

    public  static String agregarSeguroVida(java.lang.String token, int convenio, int codigoSeguro) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.agregarSeguroVida(token, convenio, codigoSeguro);
    }

    public static String consultarSeguroDeVidaEspecifico(java.lang.String token, int usuarioseguro) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.consultarSeguroDeVidaEspecifico(token, usuarioseguro);
    }

    public static String pagarSeguroVida(java.lang.String token, int usuarioseguro, long monto, java.lang.String descripcion) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.pagarSeguroVida(token, usuarioseguro, monto, descripcion);
    }

    public  static String segurosVidaPorConvenio(java.lang.String token, int convenio) {
        ws.WSFinanzas_Service service = new ws.WSFinanzas_Service();
        ws.WSFinanzas port = service.getWSFinanzasPort();
        return port.segurosVidaPorConvenio(token, convenio);    
    }   
    
}
