/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean.BeanAseguradora;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@RequestScoped
public class BeanPagoSeguroVidaxConvenio {

    /**
     * Creates a new instance of BeanPagoSeguroVidaxConvenio
     */
    
    private String token;
    private int convenio;
    
    public BeanPagoSeguroVidaxConvenio() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getConvenio() {
        return convenio;
    }

    public void setConvenio(int convenio) {
        this.convenio = convenio;
    }
    
    
    
}
