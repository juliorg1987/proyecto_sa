/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean.BeanAseguradora;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@RequestScoped
public class BeanPagarSeguroVida {

    /**
     * Creates a new instance of BeanPagarSeguroVida
     */
    
    private String token;
    private int usuarioSeguro;
    private long monto;
    private String descripcion;
    
    public BeanPagarSeguroVida() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUsuarioSeguro() {
        return usuarioSeguro;
    }

    public void setUsuarioSeguro(int usuarioSeguro) {
        this.usuarioSeguro = usuarioSeguro;
    }

    public long getMonto() {
        return monto;
    }

    public void setMonto(long monto) {
        this.monto = monto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
