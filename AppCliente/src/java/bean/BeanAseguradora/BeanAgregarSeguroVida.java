/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean.BeanAseguradora;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@RequestScoped
public class BeanAgregarSeguroVida {

    /**
     * Creates a new instance of BeanAgregarSeguroVida
     */
    
    private String token;
    private int convenio;
    private int codigoSeguro;
    
    public BeanAgregarSeguroVida() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getConvenio() {
        return convenio;
    }

    public void setConvenio(int convenio) {
        this.convenio = convenio;
    }

    public int getCodigoSeguro() {
        return codigoSeguro;
    }

    public void setCodigoSeguro(int codigoSeguro) {
        this.codigoSeguro = codigoSeguro;
    }
    
    
}
