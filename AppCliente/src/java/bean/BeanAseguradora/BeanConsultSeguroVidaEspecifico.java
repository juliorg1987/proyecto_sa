/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean.BeanAseguradora;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@RequestScoped
public class BeanConsultSeguroVidaEspecifico {

    /**
     * Creates a new instance of BeanConsultSeguroVidaEspecifico
     */
    
    private String token;
    private int usuarioSeguro;
    
    public BeanConsultSeguroVidaEspecifico() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUsuarioSeguro() {
        return usuarioSeguro;
    }

    public void setUsuarioSeguro(int usuarioSeguro) {
        this.usuarioSeguro = usuarioSeguro;
    }
    
    
}
