/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author victor
 */
@ManagedBean
@ViewScoped
public class BeanCuenta {

    private String token;
    private long dpi;
    String nombre;
    String direccion;
    String telefono;
    
    int convenio;
    
    int numcuenta;
    float monto;
    String descripcion;
    
    String mensaje;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getDpi() {
        return dpi;
    }

    public void setDpi(long dpi) {
        this.dpi = dpi;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getConvenio() {
        return convenio;
    }

    public void setConvenio(int convenio) {
        this.convenio = convenio;
    }

    public int getNumcuenta() {
        return numcuenta;
    }

    public void setNumcuenta(int numcuenta) {
        this.numcuenta = numcuenta;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMensaje() {
        String mensajeTmp = mensaje;
        mensaje = null;
        return mensajeTmp;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    
    
    /**
     * Creates a new instance of BeanCuenta
     */
    public BeanCuenta() {
    }
    
    public String crearCuenta(){
        mensaje = llamadasWSDL.aperturaDeCuenta(token, dpi, nombre, direccion, telefono);
        return null;
    }
    
    public String consultarCuenta(){
        mensaje = llamadasWSDL.consultarCuenta(token, numcuenta);
        return null;
    }
    
    public String depositos(){
        mensaje = llamadasWSDL.depositos(token, numcuenta, monto, descripcion);
        return null;
    }
    
    public String cuentasPorConvenio(){
        mensaje = llamadasWSDL.cuentasPorConvenio(token, convenio);
        return null;
    }
    
}
