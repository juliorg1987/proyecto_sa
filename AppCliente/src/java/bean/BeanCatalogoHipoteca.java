/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@ViewScoped
public class BeanCatalogoHipoteca {

    private String token;
    private long area;
    private long precioBase;
    
    public BeanCatalogoHipoteca() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getArea() {
        return area;
    }

    public void setArea(long area) {
        this.area = area;
    }

    public long getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(long precioBase) {
        this.precioBase = precioBase;
    }
    
    public String consultarCatalogoHipoteca(){
        llamadasWSDL.consultarHipotecaDelCatalogo(token, area, precioBase);
        return "menu";
    }
    
    
}
