/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Roberto
 */
@ManagedBean
@SessionScoped
public class BeanCrearConvenio implements Serializable {
    
    private BeanUsuario beanUsuario;
    
    private long dpi;
    private String  primerNombre;
    private String  segundoNombre;
    private String  primerApellido;
    private String  segundoApellido;
    private String  password;
    private String  password2;
    
    private String mensaje;
    
    /**
     * Creates a new instance of BeanCrearConvenio
     */
    public BeanCrearConvenio() {
        beanUsuario = (BeanUsuario) ((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false)).getAttribute("beanUsuario");
    }

    public long getDpi() {
        return dpi;
    }

    public void setDpi(long dpi) {
        this.dpi = dpi;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getMensaje() {
        String mensajeTmp = mensaje;
        mensaje = null;
        return mensajeTmp;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    public String guardarUsuario()
    {
        if(password.equals(password2))
        {
            mensaje = llamadasWSDL.crearConvenio(dpi, primerNombre, segundoNombre, primerApellido, segundoApellido, password);
            
            return "menu";
        }
        else
        return null;
    }

}
