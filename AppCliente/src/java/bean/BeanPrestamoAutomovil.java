/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Roberto
 */
@ManagedBean
@ViewScoped
public class BeanPrestamoAutomovil {
    

    private String token;
    private int convenio;
    private short modelo;
    private String placa;
    private String chasis;
    private long monto;
    private int numpagos;
    private short interes;
            
    public BeanPrestamoAutomovil() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getConvenio() {
        return convenio;
    }

    public void setConvenio(int convenio) {
        this.convenio = convenio;
    }

    public short getModelo() {
        return modelo;
    }

    public void setModelo(short modelo) {
        this.modelo = modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public long getMonto() {
        return monto;
    }

    public void setMonto(long monto) {
        this.monto = monto;
    }

    public int getNumpagos() {
        return numpagos;
    }

    public void setNumpagos(int numpagos) {
        this.numpagos = numpagos;
    }

    public short getInteres() {
        return interes;
    }

    public void setInteres(short interes) {
        this.interes = interes;
    }
    
    public String realizarPrestamoAutomovil(){
        llamadasWSDL.realizarPrestamoAutomovil(token, convenio, modelo, placa, chasis, monto, numpagos, interes);
        return "menu";
    }

   
    
}
